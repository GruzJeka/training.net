﻿using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Mvc;
using System.Web.Routing;
using AuctionStore.UI.App_Start;
using AuctionStore.UI.Infrastructure;

namespace AuctionStore.UI
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            HttpConfiguration config = GlobalConfiguration.Configuration;
            config.Services.Replace(typeof(IHttpControllerActivator), new DefaultApiControllerActivator());
          
            GlobalConfiguration.Configure(WebApiConfig.Register);
            ControllerBuilder.Current.SetControllerFactory(new DefaultControllerFactory(new DefaultControllerActivator()));
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            AutoMapperConfig.RegisterMaps();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
        }
    }
}
