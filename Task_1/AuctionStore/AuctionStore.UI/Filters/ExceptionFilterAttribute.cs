﻿using System.Web.Mvc;
using NLog;
using FilterAttribute = System.Web.Http.Filters.FilterAttribute;
using IExceptionFilter = System.Web.Mvc.IExceptionFilter;

namespace AuctionStore.UI.Filters
{
    public class ExceptionFilterAttribute : FilterAttribute, IExceptionFilter
    {
        private readonly ILogger _logger;

        public ExceptionFilterAttribute(ILogger logger)
        {
            _logger = logger;
        }

        public void OnException(ExceptionContext filterContext)
        {
            _logger.Warn(filterContext.Exception, filterContext.Exception.Message);
        }
    }
}