﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.42000
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AuctionStore.UI.UserService {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="UserService.IWcfUserService")]
    public interface IWcfUserService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfUserService/GetUsers", ReplyAction="http://tempuri.org/IWcfUserService/GetUsersResponse")]
        AuctionStore.Business.Entities.User[] GetUsers();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfUserService/GetUsers", ReplyAction="http://tempuri.org/IWcfUserService/GetUsersResponse")]
        System.Threading.Tasks.Task<AuctionStore.Business.Entities.User[]> GetUsersAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfUserService/GetClaims", ReplyAction="http://tempuri.org/IWcfUserService/GetClaimsResponse")]
        System.Security.Claims.Claim[] GetClaims(string userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfUserService/GetClaims", ReplyAction="http://tempuri.org/IWcfUserService/GetClaimsResponse")]
        System.Threading.Tasks.Task<System.Security.Claims.Claim[]> GetClaimsAsync(string userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfUserService/Register", ReplyAction="http://tempuri.org/IWcfUserService/RegisterResponse")]
        AuctionStore.Business.Models.OperationResult Register(AuctionStore.Business.Entities.User user);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfUserService/Register", ReplyAction="http://tempuri.org/IWcfUserService/RegisterResponse")]
        System.Threading.Tasks.Task<AuctionStore.Business.Models.OperationResult> RegisterAsync(AuctionStore.Business.Entities.User user);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfUserService/Find", ReplyAction="http://tempuri.org/IWcfUserService/FindResponse")]
        AuctionStore.Business.Entities.User Find(string login, string password);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfUserService/Find", ReplyAction="http://tempuri.org/IWcfUserService/FindResponse")]
        System.Threading.Tasks.Task<AuctionStore.Business.Entities.User> FindAsync(string login, string password);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfUserService/FindUserById", ReplyAction="http://tempuri.org/IWcfUserService/FindUserByIdResponse")]
        AuctionStore.Business.Entities.User FindUserById(string id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfUserService/FindUserById", ReplyAction="http://tempuri.org/IWcfUserService/FindUserByIdResponse")]
        System.Threading.Tasks.Task<AuctionStore.Business.Entities.User> FindUserByIdAsync(string id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfUserService/FindUserByName", ReplyAction="http://tempuri.org/IWcfUserService/FindUserByNameResponse")]
        AuctionStore.Business.Entities.User FindUserByName(string name);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfUserService/FindUserByName", ReplyAction="http://tempuri.org/IWcfUserService/FindUserByNameResponse")]
        System.Threading.Tasks.Task<AuctionStore.Business.Entities.User> FindUserByNameAsync(string name);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfUserService/UpdateUserProfile", ReplyAction="http://tempuri.org/IWcfUserService/UpdateUserProfileResponse")]
        AuctionStore.Business.Models.OperationResult UpdateUserProfile(AuctionStore.Business.Models.UserProfileModel model);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfUserService/UpdateUserProfile", ReplyAction="http://tempuri.org/IWcfUserService/UpdateUserProfileResponse")]
        System.Threading.Tasks.Task<AuctionStore.Business.Models.OperationResult> UpdateUserProfileAsync(AuctionStore.Business.Models.UserProfileModel model);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfUserService/AddToRole", ReplyAction="http://tempuri.org/IWcfUserService/AddToRoleResponse")]
        AuctionStore.Business.Models.OperationResult AddToRole(string userId, AuctionStore.DAL.EnumsDto.Role role);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfUserService/AddToRole", ReplyAction="http://tempuri.org/IWcfUserService/AddToRoleResponse")]
        System.Threading.Tasks.Task<AuctionStore.Business.Models.OperationResult> AddToRoleAsync(string userId, AuctionStore.DAL.EnumsDto.Role role);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfUserService/RemoveFromRole", ReplyAction="http://tempuri.org/IWcfUserService/RemoveFromRoleResponse")]
        AuctionStore.Business.Models.OperationResult RemoveFromRole(string userId, AuctionStore.DAL.EnumsDto.Role role);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfUserService/RemoveFromRole", ReplyAction="http://tempuri.org/IWcfUserService/RemoveFromRoleResponse")]
        System.Threading.Tasks.Task<AuctionStore.Business.Models.OperationResult> RemoveFromRoleAsync(string userId, AuctionStore.DAL.EnumsDto.Role role);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfUserService/DeleteUser", ReplyAction="http://tempuri.org/IWcfUserService/DeleteUserResponse")]
        AuctionStore.Business.Models.OperationResult DeleteUser(string userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfUserService/DeleteUser", ReplyAction="http://tempuri.org/IWcfUserService/DeleteUserResponse")]
        System.Threading.Tasks.Task<AuctionStore.Business.Models.OperationResult> DeleteUserAsync(string userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfUserService/GetClaimsIdentity", ReplyAction="http://tempuri.org/IWcfUserService/GetClaimsIdentityResponse")]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(System.Collections.Generic.Dictionary<string, string>))]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(string[]))]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(AuctionStore.Business.Models.OperationResult))]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(AuctionStore.Business.Models.UserProfileModel))]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(AuctionStore.Business.Entities.User[]))]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(AuctionStore.Business.Entities.User))]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(AuctionStore.Business.Entities.UserClaim[]))]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(AuctionStore.Business.Entities.UserClaim))]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(AuctionStore.Business.Entities.UserRole[]))]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(AuctionStore.Business.Entities.UserRole))]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(AuctionStore.DAL.EnumsDto.Role))]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(AuctionStore.DAL.EnumsDto.Locale))]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(System.Security.Claims.Claim[]))]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(System.Security.Claims.Claim))]
        System.Security.Claims.ClaimsIdentity GetClaimsIdentity(string userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfUserService/GetClaimsIdentity", ReplyAction="http://tempuri.org/IWcfUserService/GetClaimsIdentityResponse")]
        System.Threading.Tasks.Task<System.Security.Claims.ClaimsIdentity> GetClaimsIdentityAsync(string userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfUserService/AddClaim", ReplyAction="http://tempuri.org/IWcfUserService/AddClaimResponse")]
        AuctionStore.Business.Models.OperationResult AddClaim(string userId, System.Security.Claims.Claim claim);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfUserService/AddClaim", ReplyAction="http://tempuri.org/IWcfUserService/AddClaimResponse")]
        System.Threading.Tasks.Task<AuctionStore.Business.Models.OperationResult> AddClaimAsync(string userId, System.Security.Claims.Claim claim);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfUserService/RemoveClaim", ReplyAction="http://tempuri.org/IWcfUserService/RemoveClaimResponse")]
        AuctionStore.Business.Models.OperationResult RemoveClaim(string userId, System.Security.Claims.Claim claim);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfUserService/RemoveClaim", ReplyAction="http://tempuri.org/IWcfUserService/RemoveClaimResponse")]
        System.Threading.Tasks.Task<AuctionStore.Business.Models.OperationResult> RemoveClaimAsync(string userId, System.Security.Claims.Claim claim);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfUserService/ChangePassword", ReplyAction="http://tempuri.org/IWcfUserService/ChangePasswordResponse")]
        AuctionStore.Business.Models.OperationResult ChangePassword(string userId, string newPassword);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfUserService/ChangePassword", ReplyAction="http://tempuri.org/IWcfUserService/ChangePasswordResponse")]
        System.Threading.Tasks.Task<AuctionStore.Business.Models.OperationResult> ChangePasswordAsync(string userId, string newPassword);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IWcfUserServiceChannel : AuctionStore.UI.UserService.IWcfUserService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class WcfUserServiceClient : System.ServiceModel.ClientBase<AuctionStore.UI.UserService.IWcfUserService>, AuctionStore.UI.UserService.IWcfUserService {
        
        public WcfUserServiceClient() {
        }
        
        public WcfUserServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public WcfUserServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public WcfUserServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public WcfUserServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public AuctionStore.Business.Entities.User[] GetUsers() {
            return base.Channel.GetUsers();
        }
        
        public System.Threading.Tasks.Task<AuctionStore.Business.Entities.User[]> GetUsersAsync() {
            return base.Channel.GetUsersAsync();
        }
        
        public System.Security.Claims.Claim[] GetClaims(string userId) {
            return base.Channel.GetClaims(userId);
        }
        
        public System.Threading.Tasks.Task<System.Security.Claims.Claim[]> GetClaimsAsync(string userId) {
            return base.Channel.GetClaimsAsync(userId);
        }
        
        public AuctionStore.Business.Models.OperationResult Register(AuctionStore.Business.Entities.User user) {
            return base.Channel.Register(user);
        }
        
        public System.Threading.Tasks.Task<AuctionStore.Business.Models.OperationResult> RegisterAsync(AuctionStore.Business.Entities.User user) {
            return base.Channel.RegisterAsync(user);
        }
        
        public AuctionStore.Business.Entities.User Find(string login, string password) {
            return base.Channel.Find(login, password);
        }
        
        public System.Threading.Tasks.Task<AuctionStore.Business.Entities.User> FindAsync(string login, string password) {
            return base.Channel.FindAsync(login, password);
        }
        
        public AuctionStore.Business.Entities.User FindUserById(string id) {
            return base.Channel.FindUserById(id);
        }
        
        public System.Threading.Tasks.Task<AuctionStore.Business.Entities.User> FindUserByIdAsync(string id) {
            return base.Channel.FindUserByIdAsync(id);
        }
        
        public AuctionStore.Business.Entities.User FindUserByName(string name) {
            return base.Channel.FindUserByName(name);
        }
        
        public System.Threading.Tasks.Task<AuctionStore.Business.Entities.User> FindUserByNameAsync(string name) {
            return base.Channel.FindUserByNameAsync(name);
        }
        
        public AuctionStore.Business.Models.OperationResult UpdateUserProfile(AuctionStore.Business.Models.UserProfileModel model) {
            return base.Channel.UpdateUserProfile(model);
        }
        
        public System.Threading.Tasks.Task<AuctionStore.Business.Models.OperationResult> UpdateUserProfileAsync(AuctionStore.Business.Models.UserProfileModel model) {
            return base.Channel.UpdateUserProfileAsync(model);
        }
        
        public AuctionStore.Business.Models.OperationResult AddToRole(string userId, AuctionStore.DAL.EnumsDto.Role role) {
            return base.Channel.AddToRole(userId, role);
        }
        
        public System.Threading.Tasks.Task<AuctionStore.Business.Models.OperationResult> AddToRoleAsync(string userId, AuctionStore.DAL.EnumsDto.Role role) {
            return base.Channel.AddToRoleAsync(userId, role);
        }
        
        public AuctionStore.Business.Models.OperationResult RemoveFromRole(string userId, AuctionStore.DAL.EnumsDto.Role role) {
            return base.Channel.RemoveFromRole(userId, role);
        }
        
        public System.Threading.Tasks.Task<AuctionStore.Business.Models.OperationResult> RemoveFromRoleAsync(string userId, AuctionStore.DAL.EnumsDto.Role role) {
            return base.Channel.RemoveFromRoleAsync(userId, role);
        }
        
        public AuctionStore.Business.Models.OperationResult DeleteUser(string userId) {
            return base.Channel.DeleteUser(userId);
        }
        
        public System.Threading.Tasks.Task<AuctionStore.Business.Models.OperationResult> DeleteUserAsync(string userId) {
            return base.Channel.DeleteUserAsync(userId);
        }
        
        public System.Security.Claims.ClaimsIdentity GetClaimsIdentity(string userId) {
            return base.Channel.GetClaimsIdentity(userId);
        }
        
        public System.Threading.Tasks.Task<System.Security.Claims.ClaimsIdentity> GetClaimsIdentityAsync(string userId) {
            return base.Channel.GetClaimsIdentityAsync(userId);
        }
        
        public AuctionStore.Business.Models.OperationResult AddClaim(string userId, System.Security.Claims.Claim claim) {
            return base.Channel.AddClaim(userId, claim);
        }
        
        public System.Threading.Tasks.Task<AuctionStore.Business.Models.OperationResult> AddClaimAsync(string userId, System.Security.Claims.Claim claim) {
            return base.Channel.AddClaimAsync(userId, claim);
        }
        
        public AuctionStore.Business.Models.OperationResult RemoveClaim(string userId, System.Security.Claims.Claim claim) {
            return base.Channel.RemoveClaim(userId, claim);
        }
        
        public System.Threading.Tasks.Task<AuctionStore.Business.Models.OperationResult> RemoveClaimAsync(string userId, System.Security.Claims.Claim claim) {
            return base.Channel.RemoveClaimAsync(userId, claim);
        }
        
        public AuctionStore.Business.Models.OperationResult ChangePassword(string userId, string newPassword) {
            return base.Channel.ChangePassword(userId, newPassword);
        }
        
        public System.Threading.Tasks.Task<AuctionStore.Business.Models.OperationResult> ChangePasswordAsync(string userId, string newPassword) {
            return base.Channel.ChangePasswordAsync(userId, newPassword);
        }
    }
}
