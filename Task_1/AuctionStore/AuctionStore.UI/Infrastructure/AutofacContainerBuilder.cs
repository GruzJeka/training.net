﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Web.Hosting;
using AuctionStore.Business.Abstract;
using AuctionStore.Business.Enums;
using AuctionStore.Business.Infrastructure;
using AuctionStore.Business.Repositories;
using AuctionStore.Business.Services;
using AuctionStore.DAL.Abstract;
using AuctionStore.DAL.Context;
using AuctionStore.DAL.DtoEntities;
using AuctionStore.DAL.Repositories;
using AuctionStore.UI.UserService;
using Autofac;
using Autofac.Integration.Mvc;
using NLog;

namespace AuctionStore.UI.Infrastructure
{
    public class AutofacContainerBuilder
    {
        private readonly ContainerBuilder _builder;

        private readonly IContainer _container;

        public AutofacContainerBuilder(DataType type, string location)
        {
            _builder = new ContainerBuilder();
            _builder.RegisterControllers(typeof(MvcApplication).Assembly);
            RegisterStatic();
            RegisterServices();
            switch (type)
            {
                case DataType.Json:
                    {
                        RegisterJsonRepositories(location);
                        break;
                    }

                case DataType.Sql:
                    {
                        RegisterSqlRepositories(location);
                        break;
                    }
            }

            _container = _builder.Build();
        }

        public object Resolve(Type type)
        {
            return _container.Resolve(type);
        }

        private void RegisterSqlRepositories(string connectionString)
        {
            _builder.RegisterType<EfAuctionHouseDbContext>()
                .As<DbContext>()
                .WithParameter("connectionString", connectionString);

            _builder.RegisterType<EfRepository<CategoryDto>>()
                .As<IRepository<CategoryDto>>();

            _builder.RegisterType<EfRepository<ProductDto>>()
                .As<IRepository<ProductDto>>();

            _builder.RegisterType<EfRepository<BidDto>>()
                .As<IRepository<BidDto>>();
        }

        private void RegisterJsonRepositories(string connectionString)
        {
            string basePath = HostingEnvironment.MapPath($@"~/App_Data/{connectionString}/");
            
            _builder.RegisterType<JsonContext<ProductDto>>()
                .As<IContext<ProductDto>>()
                .WithParameter("path", basePath + ConfigurationManager.AppSettings["ProductsPath"]);

            _builder.RegisterType<JsonContext<BidDto>>()
                .As<IContext<BidDto>>()
                .WithParameter("path", basePath + ConfigurationManager.AppSettings["BidsPath"]);

            _builder.RegisterType<JsonContext<CategoryDto>>()
                .As<IContext<CategoryDto>>()
                .WithParameter("path", basePath + ConfigurationManager.AppSettings["CategoriesPath"]);

            _builder.RegisterType<FileRepository<ProductDto>>().As<IRepository<ProductDto>>();
            _builder.RegisterType<FileRepository<BidDto>>().As<IRepository<BidDto>>();
            _builder.RegisterType<FileRepository<CategoryDto>>().As<IRepository<CategoryDto>>();
        }

        private void RegisterServices()
        {
            _builder.RegisterType<CategoryService>()
                .As<ICategoryService>();

            _builder.RegisterType<ProductService>()
                .As<IProductService>();

            _builder.RegisterType<BidService>()
                .As<IBidService>();

            _builder.RegisterType<BidPriceCaluclator>()
               .As<IBidPriceCalculator>()
               .WithParameter("percent", decimal.Parse(ConfigurationManager.AppSettings["Percent"]));
        }

        private void RegisterStatic()
        {
            Logger logger = LogManager.GetCurrentClassLogger();
            _builder.RegisterInstance(logger).As<ILogger>();

            _builder.RegisterType<AuctionHousesWebConfigStore>().As<IAuctionStore>();

            _builder.RegisterType<WcfUserServiceClient>();
        }
    }
}