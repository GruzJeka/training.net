﻿using System;
using System.Linq;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;
using System.Web.Mvc;
using System.Web.Routing;
using AuctionStore.Business.Entities;
using AuctionStore.Business.Repositories;
using NLog;

namespace AuctionStore.UI.Infrastructure
{
    public class DefaultApiControllerActivator : IHttpControllerActivator
    {
        private AuctionHousesWebConfigStore Houses => new AuctionHousesWebConfigStore(LogManager.GetCurrentClassLogger());

        public IHttpController Create(HttpRequestMessage request, HttpControllerDescriptor controllerDescriptor, Type controllerType)
        {
            AutofacApiContainerBuilder builder;

            if (request.GetRequestContext().RouteData.Values["auction"] != null)
            {
                string auctionName = request.GetRequestContext().RouteData.Values["auction"].ToString();
                AuctionHouse house = Houses.Get(auctionName);
                if (house != null)
                {
                    builder = new AutofacApiContainerBuilder(house.Type, house.Location);
                    return builder.Resolve(controllerType) as IHttpController;
                }

                throw new NullReferenceException("Not found auction house");
            }

            builder = new AutofacApiContainerBuilder();
            return builder.Resolve(controllerType) as IHttpController;
        }
    }
}