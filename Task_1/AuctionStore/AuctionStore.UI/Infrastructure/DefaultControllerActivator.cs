﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using AuctionStore.Business.Entities;
using AuctionStore.Business.Repositories;
using NLog;

namespace AuctionStore.UI.Infrastructure
{
    public class DefaultControllerActivator : IControllerActivator
    {
        private AuctionHousesWebConfigStore Houses => new AuctionHousesWebConfigStore(LogManager.GetCurrentClassLogger());

        public IController Create(RequestContext requestContext, Type controllerType)
        {
            AuctionHouse house;
            if (requestContext.RouteData.Values["auction"] == null)
            {
                house = Houses.GetAll().First();
                requestContext.RouteData.Values["auction"] = house.Name;
            }
            else
            {
                string auctionName = (string)requestContext.RouteData.Values["auction"];
                house = Houses.Get(auctionName);
            }

            AutofacContainerBuilder builder = new AutofacContainerBuilder(house.Type, house.Location);

            return builder.Resolve(controllerType) as IController;
        }
    }
}