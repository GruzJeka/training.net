﻿using System;
using System.Linq;
using AuctionStore.Business.Entities;
using AuctionStore.Business.Enums;
using AuctionStore.Business.Models;
using AuctionStore.DAL.ConfigEntities;
using AuctionStore.DAL.DtoEntities;
using AuctionStore.DAL.EnumsDto;
using AuctionStore.UI.Models;
using AuctionStore.UI.Models.BidViewModels;
using AuctionStore.UI.Models.CategoriesViewModels;
using AuctionStore.UI.Models.EnumsViewModels;
using AuctionStore.UI.Models.ProductsViewModels;
using AuctionStore.UI.Models.UserViewModels;
using AutoMapper;

namespace AuctionStore.UI.Infrastructure
{
    public class AutoMapperConfig
    {
        public static void RegisterMaps()
        {
#pragma warning disable 618
            ////EfEntites
            Mapper.CreateMap<UserProfileViewModel, UserProfileModel>();
            Mapper.CreateMap<LoginViewModel, LoginModel>();

            Mapper.CreateMap<Category, CategoryDto>();
            Mapper.CreateMap<CategoryDto, Category>();

            Mapper.CreateMap<User, UserDto>();
            Mapper.CreateMap<UserDto, User>();

            Mapper.CreateMap<Product, ProductDto>()
                .ForMember("Duration", opt => opt.MapFrom(c => c.Duration.Ticks));
            Mapper.CreateMap<ProductDto, Product>()
                .ForMember("Duration", opt => opt.MapFrom(c => TimeSpan.FromTicks(c.Duration)));

            Mapper.CreateMap<UserRole, UserRoleDto>();
            Mapper.CreateMap<UserRoleDto, UserRole>();

            Mapper.CreateMap<BidDto, Bid>();
            Mapper.CreateMap<Bid, BidDto>().ForSourceMember(p => p.Product, opt => opt.Ignore());

            Mapper.CreateMap<UserClaim, UserClaimDto>();
            Mapper.CreateMap<UserClaimDto, UserClaim>();

            Mapper.CreateMap<AuctionHouse, AuctionHouseUpdateViewModel>();
            Mapper.CreateMap<AuctionHouseUpdateViewModel, AuctionHouse>();

            ////ViewModels
            Mapper.CreateMap<ProductCreateViewModel, Product>();
            Mapper.CreateMap<Product, ProductCreateViewModel>();

            Mapper.CreateMap<Category, CategoryViewModel>();
            Mapper.CreateMap<CategoryViewModel, Category>();

            Mapper.CreateMap<UpdateUserViewModel, User>();
            Mapper.CreateMap<User, UpdateUserViewModel>();

            Mapper.CreateMap<Product, ProductUpdateViewModel>();
            Mapper.CreateMap<ProductUpdateViewModel, Product>();

            Mapper.CreateMap<Product, ProductViewModel>()
              .ForMember("TimeLeft", opt => opt.MapFrom(c => c.GetTimeLeft()))
              .ForMember("IsActual", opt => opt.MapFrom(c => c.IsActual()))
              .ForMember("State", opt => opt.MapFrom(c => Mapper.Map<ProductState, ProductStateView>(c.State)));
            Mapper.CreateMap<ProductViewModel, Product>();

            Mapper.CreateMap<Bid, BidViewModel>()
                .ForMember("State", opt => opt.MapFrom(c => Mapper.Map<BidState, BidStateView>(c.State)));

            Mapper.CreateMap<CreateUserViewModel, User>();
            Mapper.CreateMap<User, CreateUserViewModel>();

            Mapper.CreateMap<LoginViewModel, User>();

            Mapper.CreateMap<User, UserProfileViewModel>();
            Mapper.CreateMap<User, UserViewModel>()
                .ForMember("UserRoles", opt => opt.MapFrom(c => c.UserRoles.Select(y => y.Role)));

            Mapper.CreateMap<UserRoleView, UserRole>();
            Mapper.CreateMap<UserRole, UserRoleView>();

            ////ConfigEntities
            Mapper.CreateMap<AuctionHouse, ConfigAuctionHouse>();
            Mapper.CreateMap<ConfigAuctionHouse, AuctionHouse>();

            Mapper.CreateMap<AuctionHouse, AuctionHouseViewModel>();
            Mapper.CreateMap<AuctionHouseViewModel, AuctionHouse>();

#pragma warning restore 618
        }
    }
}