﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web.Mvc;
using AuctionStore.Business.Entities;

namespace AuctionStore.UI.Helpers
{
    public class CategoriesHelper
    {
        public static IEnumerable<SelectListItem> GetCategoriesSelectListItems(IEnumerable<Category> categories)
        {
            return categories.Select(c => new SelectListItem
            {
                Value = c.Id.ToString(),
                Text = c.Name
            });
        }
    }
}