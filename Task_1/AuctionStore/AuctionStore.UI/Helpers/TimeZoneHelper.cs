﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace AuctionStore.UI.Helpers
{
    public static class TimeZoneHelper
    {
        public static MvcHtmlString DisplayDateTime(this HtmlHelper html, DateTime dateTime, string timeZoneId)
        {
            TimeZoneInfo zone = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
            DateTime date = TimeZoneInfo.ConvertTime(dateTime, zone);
            return MvcHtmlString.Create(date.ToString());
        }

        public static IEnumerable<SelectListItem> GetTimeZonesSelectListItems()
        {
            return TimeZoneInfo.GetSystemTimeZones().Select(s => new SelectListItem
            {
                Value = s.Id,
                Text = s.DisplayName
            });
        }
    }
}