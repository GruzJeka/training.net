﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using AuctionStore.Business.Entities;

namespace AuctionStore.UI.Helpers
{
    public static class SelectListItemHelper
    {
        public static IEnumerable<SelectListItem> CategoriesForModerator(IEnumerable<Category> categories, IEnumerable<Claim> moderatorClaims, string currentAuctionName)
        {
            IEnumerable<string> allCategoriesNames = categories.Select(a => a.Name);
            IEnumerable<string> moderatorCategoriesNames =
                 moderatorClaims.Where(c => c.Type == ClaimType.CategoryManage && c.Value.Contains(currentAuctionName))
                     .Select(p => p.Value.Split('/')[1]);

            IEnumerable<string> exceptCategories = allCategoriesNames.Except(moderatorCategoriesNames);

            return exceptCategories.Select(c => new SelectListItem
            {
                Value = c,
                Text = c
            });
        } 
    }
}