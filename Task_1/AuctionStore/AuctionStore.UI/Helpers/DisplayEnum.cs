﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.Web.Mvc;

namespace AuctionStore.UI.Helpers
{
    public static class DisplayEnum
    {
        public static MvcHtmlString DisplayForEnum(this HtmlHelper html, Enum value)
        {
            Type enumType = value.GetType(); 
            var enumValue = Enum.GetName(enumType, value);
            MemberInfo member = enumType.GetMember(enumValue)[0];

            var attrs = member.GetCustomAttributes(typeof(DisplayAttribute), false);
            var outString = ((DisplayAttribute)attrs[0]).Name;

            if (((DisplayAttribute)attrs[0]).ResourceType != null)
            {
                outString = ((DisplayAttribute)attrs[0]).GetName();
            }

            return MvcHtmlString.Create(outString);
        }
    }
}