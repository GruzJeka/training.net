﻿using System.Web.Http;

namespace AuctionStore.UI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                string.Empty,
                "api/{controller}/{auction}Auction/id{id}");

            config.Routes.MapHttpRoute(
                "DefaultApi",
                "api/{controller}/{auction}Auction/{category}",
                new { category = RouteParameter.Optional });
        }
    }
}
