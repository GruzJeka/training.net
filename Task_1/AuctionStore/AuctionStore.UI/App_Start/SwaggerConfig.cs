using System;
using System.Web.Http;
using AuctionStore.UI;
using Swashbuckle.Application;
using WebActivatorEx;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace AuctionStore.UI
{
    public class SwaggerConfig
    {
        public static void Register()
        {
            Swashbuckle.Bootstrapper.Init(GlobalConfiguration.Configuration);

            SwaggerSpecConfig.Customize(c => c.IncludeXmlComments(GetXmlComments()));
        }

        private static string GetXmlComments()
        {
            return $@"{AppDomain.CurrentDomain.BaseDirectory}\bin\AuctionStore.UI.XML";
        }
    }
}