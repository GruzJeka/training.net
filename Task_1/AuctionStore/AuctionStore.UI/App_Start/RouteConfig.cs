﻿using System.Web.Mvc;
using System.Web.Routing;

namespace AuctionStore.UI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            //routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                null,
                string.Empty,
                new { controller = "Product", action = "ProductsList", category = (string)null, page = 1, auction = (string)null });

            routes.MapRoute(
                null,
                "{auction}Auction",
                new { controller = "Product", action = "ProductsList", category = (string)null, page = 1 });

            routes.MapRoute(
                null,
                "{auction}Auction/Page{page}",
                new { controller = "Product", action = "ProductsList", category = (string)null });

            routes.MapRoute(
                null,
                "{auction}Auction/{category}",
                new { controller = "Product", action = "ProductsList", page = 1 });

            routes.MapRoute(
                null,
                "{auction}Auction/{category}/Page{page}",
                new { controller = "Product", action = "ProductsList" });

            routes.MapRoute(
              "stateRoute",
              "{auction}Auction/{controller}/{action}/{state}State");

            routes.MapRoute(
               "route",
               "{auction}Auction/{controller}/{action}/{id}",
               new { id = UrlParameter.Optional });

            routes.MapRoute(
                null,
                "{controller}/{action}/{id}",
                new { id = UrlParameter.Optional });

            ////routes.MapRoute("StateRoute", "{controller}/{action}/{state}");

            routes.MapRoute(null, "{auction}Auction/{controller}/{action}");
        }
    }
}
