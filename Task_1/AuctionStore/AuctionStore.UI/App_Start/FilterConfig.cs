﻿using System.Web.Mvc;
using AuctionStore.UI.Filters;
using NLog;

namespace AuctionStore.UI.App_Start
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new CultureAttribute());
            filters.Add(new ExceptionFilterAttribute(LogManager.GetCurrentClassLogger()));
        }
    }
}