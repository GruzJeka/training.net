﻿using System.ComponentModel.DataAnnotations;
using DataType = AuctionStore.Business.Enums.DataType;

namespace AuctionStore.UI.Models
{
    public class AuctionHouseViewModel
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Location { get; set; }

        public DataType Type { get; set; }
    }
}