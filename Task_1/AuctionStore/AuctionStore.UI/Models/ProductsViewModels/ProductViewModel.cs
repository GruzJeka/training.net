﻿using System;
using System.ComponentModel.DataAnnotations;
using AuctionStore.Business.Entities;
using AuctionStore.UI.Models.CategoriesViewModels;
using AuctionStore.UI.Models.EnumsViewModels;
using Rsc;

namespace AuctionStore.UI.Models.ProductsViewModels
{
    public class ProductViewModel
    {
        public Guid Id { get; set; }

        [Display(ResourceType = typeof(Resource), Name = "Name")]
        public string Name { get; set; }

        [Display(ResourceType = typeof(Resource), Name = "Description")]
        public string Description { get; set; }

        [Display(ResourceType = typeof(Resource), Name = "Duration")]
        public TimeSpan Duration { get; set; }

        [Display(ResourceType = typeof(Resource), Name = "StartPrice")]
        public decimal StartPrice { get; set; }

        public CategoryViewModel Category { get; set; }
        [Display(ResourceType = typeof(Resource), Name = "StartDate")]
        public DateTime StartDate { get; set; }

        [Display(ResourceType = typeof(Resource), Name = "State")]
        public ProductStateView State { get; set; }

        [Display(ResourceType = typeof(Resource), Name = "TimeLeft")]
        [DisplayFormat(DataFormatString = "{0:hh\\:mm}", ApplyFormatInEditMode = true)]
        public TimeSpan TimeLeft { get; set; }

        [Display(ResourceType = typeof(Resource), Name = "Image")]
        public string ImageData { get; set; }

        public string ImageType { get; set; }

        [Display(ResourceType = typeof(Resource), Name = "BidsQuantity")]
        public int BidsQuantity { get; set; }

        public decimal BidPrice { get; set; }

        public Guid UserId { get; set; }

        public bool IsActual { get; set; }
    }
}