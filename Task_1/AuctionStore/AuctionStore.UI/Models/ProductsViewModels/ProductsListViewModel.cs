﻿using System.Collections.Generic;

namespace AuctionStore.UI.Models.ProductsViewModels
{
    public class ProductsListViewModel
    {
        public IEnumerable<ProductViewModel> Products { get; set; }

        public PagingInfo PagingInfo { get; set; }

        public string CurrentCategory { get; set; }
    }
}