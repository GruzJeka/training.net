﻿using System;
using System.ComponentModel.DataAnnotations;
using Rsc;

namespace AuctionStore.UI.Models.ProductsViewModels
{
    public class ProductUpdateViewModel
    {
        public Guid Id { get; set; }

        public Guid UserId { get; set; }

        public DateTime StartDate { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 1, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "WrongNameLength")]
        [Display(ResourceType = typeof(Resource), Name = "Name")]
        public string Name { get; set; }

        [Required]
        [StringLength(500, MinimumLength = 1, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "WrongDescriptionLength")]
        [DataType(DataType.MultilineText)]
        [Display(ResourceType = typeof(Resource), Name = "Description")]
        public string Description { get; set; }

        [Required]
        [Display(ResourceType = typeof(Resource), Name = "Duration")]
        public TimeSpan Duration { get; set; }

        [Required]
        [Range(typeof(decimal), "10", "2000000", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "WrongStartPrice")]
        [Display(ResourceType = typeof(Resource), Name = "StartPrice")]
        [DisplayFormat(DataFormatString = "{0}", ApplyFormatInEditMode = true)]
        public decimal StartPrice { get; set; }

        [Required]
        public Guid CategoryId { get; set; }

        public string ImageData { get; set; }

        public string ImageType { get; set; }
    }
}