﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Web.Mvc;

namespace AuctionStore.UI.Models
{
    public class ModeratorCategoriesManageViewModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public IEnumerable<SelectListItem> Categories { get; set; }

        public IEnumerable<Claim> Claims { get; set; } 
    }
}