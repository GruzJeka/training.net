﻿using System.ComponentModel.DataAnnotations;
using Rsc;

namespace AuctionStore.UI.Models.EnumsViewModels
{
    public enum ProductStateView
    {
        [Display(ResourceType = typeof(Resource), Name = "Draft")]
        Draft,
        [Display(ResourceType = typeof(Resource), Name = "Selling")]
        Selling,
        [Display(ResourceType = typeof(Resource), Name = "Banned")]
        Banned,
        [Display(ResourceType = typeof(Resource), Name = "Sold")]
        Sold
    }
}