﻿using System.ComponentModel.DataAnnotations;
using Rsc;

namespace AuctionStore.UI.Models.EnumsViewModels
{
    public enum BidStateView
    {
        [Display(ResourceType = typeof(Resource), Name = "Interrupted")]
        Interrupted,
        [Display(ResourceType = typeof(Resource), Name = "Leader")]
        Leader
    }
}