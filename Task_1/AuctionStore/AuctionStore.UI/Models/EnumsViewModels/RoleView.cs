﻿using System.ComponentModel.DataAnnotations;
using Rsc;

namespace AuctionStore.UI.Models.EnumsViewModels
{
    public enum RoleView
    {
        [Display(ResourceType = typeof(Resource), Name = "User")]
        User,
        [Display(ResourceType = typeof(Resource), Name = "Moderator")]
        Moderator,
        [Display(ResourceType = typeof(Resource), Name = "Admin")]
        Admin
    }
}