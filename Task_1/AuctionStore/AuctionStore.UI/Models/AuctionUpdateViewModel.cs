﻿using System.ComponentModel.DataAnnotations;

namespace AuctionStore.UI.Models
{
    public class AuctionHouseUpdateViewModel
    {
        [Required]
        public string Name { get; set; }
    }
}