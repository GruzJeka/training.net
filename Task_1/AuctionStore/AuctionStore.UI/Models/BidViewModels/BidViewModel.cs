﻿using System;
using System.ComponentModel.DataAnnotations;
using AuctionStore.UI.Models.EnumsViewModels;
using AuctionStore.UI.Models.ProductsViewModels;
using Rsc;

namespace AuctionStore.UI.Models.BidViewModels
{
    public class BidViewModel
    {
        public Guid Id { get; set; }

        [Display(ResourceType = typeof(Resource), Name = "Price")]
        public decimal Price { get; set; }

        [Display(ResourceType = typeof(Resource), Name = "State")]
        public BidStateView State { get; set; }

        public ProductViewModel Product { get; set; }
    }
}