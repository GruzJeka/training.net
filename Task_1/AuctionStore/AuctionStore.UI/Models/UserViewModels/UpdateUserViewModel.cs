﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using AuctionStore.Business.Enums;
using Rsc;

namespace AuctionStore.UI.Models.UserViewModels
{
    public class UpdateUserViewModel
    {
        public Guid Id { get; set; }

        public DateTime RegistrationDate { get; set; }

        [Required]
        [StringLength(25, MinimumLength = 4, ErrorMessageResourceName = "WrongUserNameLength", ErrorMessageResourceType = typeof(Resource))]
        [Display(ResourceType = typeof(Resource), Name = "UserName")]
        public string UserName { get; set; }

        [Required]
        [DataType(System.ComponentModel.DataAnnotations.DataType.Password)]
        [Compare("ConfirmPassword", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "WrongConfirmPassword")]
        [StringLength(25, MinimumLength = 4, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "WrongPasswordLength")]
        [Display(ResourceType = typeof(Resource), Name = "Password")]
        public string Password { get; set; }

        [Required]
        [DataType(System.ComponentModel.DataAnnotations.DataType.Password)]
        [Compare("Password")]
        [Display(ResourceType = typeof(Resource), Name = "ConfrimPassword")]
        public string ConfirmPassword { get; set; }

        [Required]
        [Display(ResourceType = typeof(Resource), Name = "FirstName")]
        public string FirstName { get; set; }

        [Required]
        [Display(ResourceType = typeof(Resource), Name = "LastName")]
        public string LastName { get; set; }
        ////public int TimeZoneId { get; set; }
    }
}