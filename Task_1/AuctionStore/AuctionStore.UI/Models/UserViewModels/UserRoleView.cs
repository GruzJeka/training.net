﻿using System;
using AuctionStore.UI.Models.EnumsViewModels;

namespace AuctionStore.UI.Models.UserViewModels
{
    public class UserRoleView
    {
        public Guid UserId { get; set; }

        public RoleView Role { get; set; }
    }
}