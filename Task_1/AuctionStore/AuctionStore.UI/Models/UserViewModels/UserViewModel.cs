﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using AuctionStore.UI.Models.EnumsViewModels;
using Rsc;

namespace AuctionStore.UI.Models.UserViewModels
{
    public class UserViewModel
    {
        public Guid Id { get; set; }

        [Display(ResourceType = typeof(Resource), Name = "UserName")]
        public string UserName { get; set; }

        [Display(ResourceType = typeof(Resource), Name = "FirstName")]
        public string FirstName { get; set; }

        [Display(ResourceType = typeof(Resource), Name = "LastName")]
        public string LastName { get; set; }

        [Display(ResourceType = typeof(Resource), Name = "RegistrationDate")]
        public DateTime RegistrationDate { get; set; }

        public IEnumerable<RoleView> UserRoles { get; set; }
    }
}