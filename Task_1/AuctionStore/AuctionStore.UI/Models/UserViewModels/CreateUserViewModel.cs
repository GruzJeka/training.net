﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Rsc;

namespace AuctionStore.UI.Models.UserViewModels
{
    public class CreateUserViewModel
    {
        [Required]
        [StringLength(25, MinimumLength = 4, ErrorMessageResourceName = "WrongUserNameLength", ErrorMessageResourceType = typeof(Resource))]
        [Display(ResourceType = typeof(Resource), Name = "UserName")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [System.ComponentModel.DataAnnotations.Compare("ConfirmPassword", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "WrongConfirmPassword")]
        [StringLength(25, MinimumLength = 4, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "WrongPasswordLength")]
        [Display(ResourceType = typeof(Resource), Name = "Password")]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [System.ComponentModel.DataAnnotations.Compare("Password")]
        [Display(ResourceType = typeof(Resource), Name = "ConfrimPassword")]
        public string ConfirmPassword { get; set; }

        [Required]
        [Display(ResourceType = typeof(Resource), Name = "FirstName")]
        public string FirstName { get; set; }

        [Required]
        [Display(ResourceType = typeof(Resource), Name = "LastName")]
        public string LastName { get; set; }

        [Required]
        public string TimeZoneId { get; set; }

        public IEnumerable<SelectListItem> TimeZonesSelectListItems { get; set; }
    }
}