﻿using AuctionStore.DAL.EnumsDto;

namespace AuctionStore.UI.Models.UserViewModels
{
    public class UserProfileViewModel
    {
        public string TimeZoneId { get; set; }

        public Locale Locale { get; set; }

        public ChangePasswordViewModel ChangePasswordModel { get; set; }
    }
}