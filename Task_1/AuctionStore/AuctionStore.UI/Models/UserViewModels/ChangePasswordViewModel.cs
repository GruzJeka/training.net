﻿using System.ComponentModel.DataAnnotations;
using Rsc;

namespace AuctionStore.UI.Models.UserViewModels
{
    public class ChangePasswordViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [System.ComponentModel.DataAnnotations.Compare("ConfirmPassword", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "WrongConfirmPassword")]
        [StringLength(25, MinimumLength = 4, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "WrongPasswordLength")]
        [Display(ResourceType = typeof(Resource), Name = "Password")]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [System.ComponentModel.DataAnnotations.Compare("Password")]
        [Display(ResourceType = typeof(Resource), Name = "ConfrimPassword")]
        public string ConfirmPassword { get; set; }
    }
}