﻿using System;
using System.ComponentModel.DataAnnotations;
using Rsc;

namespace AuctionStore.UI.Models.CategoriesViewModels
{
    public class CategoryViewModel
    {
        public Guid Id { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 1, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "WrongCategoryNameLength")]
        [Display(ResourceType = typeof(Resource), Name = "CategoryName")]
        public string Name { get; set; }

        [Required]
        [StringLength(200, MinimumLength = 1, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "WrongCategoryDescriptionLength")]
        [Display(ResourceType = typeof(Resource), Name = "Description")]
        public string Description { get; set; }
    }
}