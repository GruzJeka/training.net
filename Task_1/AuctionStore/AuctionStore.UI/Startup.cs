﻿using System.Web.Http;
using Autofac.Integration.WebApi;
using Microsoft.Owin;
using Owin;
using Swashbuckle;

[assembly: OwinStartup(typeof(AuctionStore.UI.Startup))]

namespace AuctionStore.UI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            HttpConfiguration configuration = new HttpConfiguration();
            Bootstrapper.Init(configuration);
        }
    }
}