﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.UI;
using AuctionStore.Business.Abstract;
using AuctionStore.Business.Entities;
using AuctionStore.DAL.EnumsDto;
using AuctionStore.UI.Helpers;
using AuctionStore.UI.Models;
using AuctionStore.UI.Models.ProductsViewModels;
using AutoMapper;
using Microsoft.AspNet.Identity;
using NLog;
using Rsc;

namespace AuctionStore.UI.Controllers
{
    [HandleError(View = "ErrorView")]
    public class ProductController : Controller
    {
        public const int PageSize = 8;

        private readonly Lazy<IProductService> _productService;

        private readonly Lazy<ICategoryService> _categoryService;

        private readonly Lazy<ILogger> _logger;

        public ProductController(Lazy<IProductService> productService, Lazy<ICategoryService> categoryService, Lazy<ILogger> logger)
        {
            _productService = productService;
            _categoryService = categoryService;
            _logger = logger;
        }

        [Authorize(Roles = "User, Admin")]
        public ViewResult Create()
        {
            ViewBag.CategoriesSelectListItem =
                CategoriesHelper.GetCategoriesSelectListItems(_categoryService.Value.All());

            return View(new ProductCreateViewModel()
            {
                Duration = TimeSpan.Parse(ConfigurationManager.AppSettings["MinDuration"])
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "User, Admin")]
        public async Task<ActionResult> Create(ProductCreateViewModel product)
        {
            if (product.CategoryId == Guid.Empty.ToString())
            {
                ModelState.AddModelError(string.Empty, Resource.DontSelectCategory);
            }

            if (ModelState.IsValid)
            {
                if (Request.Files.Count != 0)
                {
                    var file = Request.Files[0];
                    if (file?.ContentLength != 0)
                    {
                        byte[] fileBytes = new byte[file.ContentLength];
                        file.InputStream.Read(fileBytes, 0, fileBytes.Length);
                        product.ImageData = Convert.ToBase64String(fileBytes);
                        product.ImageType = file.ContentType;
                    }
                }

                Product entry = Mapper.Map<ProductCreateViewModel, Product>(product);
                await _productService.Value.Create(entry, Guid.Parse(User.Identity.GetUserId()));

                _logger.Value.Info($"product {product.Name} created by {User.Identity.Name}");
                TempData["message"] = Resource.ProductCreated + $": {product.Name}";
                return RedirectToAction("GetUserProducts");
            }

            ViewBag.CategoriesSelectListItem =
                CategoriesHelper.GetCategoriesSelectListItems(_categoryService.Value.All());
            return View(product);
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(Guid id, string returnUrl)
        {
            Product product = await _productService.Value.Get(id);
            if (product != null)
            {
                await _productService.Value.Delete(id);
                TempData["message"] = Resource.ProductDeleted;
                _logger.Value.Info($"product {product.Name} deleted by {User.Identity.Name}");
            }

            TempData["error-message"] = "Product not found";
            return Redirect(returnUrl);
        }

        [Authorize(Roles = "User, Admin")]
        public async Task<ActionResult> Update(Guid id)
        {
            if (id == Guid.Empty)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Product product = await _productService.Value.Get(id);
            ProductUpdateViewModel model = Mapper.Map<Product, ProductUpdateViewModel>(product);

            ViewBag.CategoriesSelectListItem =
                CategoriesHelper.GetCategoriesSelectListItems(_categoryService.Value.All());
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "User, Admin")]
        public async Task<ActionResult> Update(ProductUpdateViewModel product)
        {
            if (product.CategoryId == Guid.Empty)
            {
                ModelState.AddModelError(string.Empty, Resource.DontSelectCategory);
            }

            if (ModelState.IsValid)
            {
                if (Request.Files.Count != 0)
                {
                    var file = Request.Files[0];
                    if (file?.ContentLength != 0)
                    {
                        byte[] fileBytes = new byte[file.ContentLength];
                        file.InputStream.Read(fileBytes, 0, fileBytes.Length);
                        product.ImageData = Convert.ToBase64String(fileBytes);
                        product.ImageType = file.ContentType;
                    }
                }

                Product entry = Mapper.Map<ProductUpdateViewModel, Product>(product);
                await _productService.Value.Update(entry);

                TempData["message"] = Resource.ProductUpdated + $": {product.Name}";
                _logger.Value.Info($"product {product.Name} created by {User.Identity.Name}");
                return RedirectToAction("GetUserProducts");
            }

            ViewBag.CategoriesSelectListItem =
                CategoriesHelper.GetCategoriesSelectListItems(_categoryService.Value.All());
            return View(product);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin, Moderator")]
        public async Task<ActionResult> ChangeState(Guid id, ProductState newState, string returnUrl)
        {
            await _productService.Value.ChangeState(id, newState);
            return Redirect(returnUrl);
        }

        public async Task<ViewResult> ProductsList(string category = null, int page = 1)
        {
            IEnumerable<Product> products;
            int totalItems = 0;
            products = await _productService.Value.All();
            products = products.Where(p => p.IsActual()).ToList();

            if (category == null)
            {
                totalItems = products.Count();
            }
            else
            {
                Category categoryEntry = await _categoryService.Value.Get(category);
                Guid categoryId = categoryEntry.Id;

                products = products.Where(p => p.CategoryId == categoryId).ToList();
                totalItems = products.Count();
            }

            IEnumerable<ProductViewModel> productsView =
                Mapper.Map<IEnumerable<Product>, IEnumerable<ProductViewModel>>(
                    products
                    .OrderBy(p => p.StartDate)
                        .Skip((page - 1) * PageSize)
                        .Take(PageSize)
                        .ToList());

            ProductsListViewModel model = new ProductsListViewModel()
            {
                Products = productsView,
                PagingInfo = new PagingInfo()
                {
                    CurrentPage = page,
                    ItemsPerPage = PageSize,
                    TotalItems = totalItems
                },
                CurrentCategory = category
            };
            return View(model);
        }

        public async Task<ActionResult> Details(string id, string returnUrl)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            ViewBag.ReturnUrl = returnUrl;

            Product product = await _productService.Value.Get(Guid.Parse(id));
            if (!product.IsActual())
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            return View(Mapper.Map<Product, ProductViewModel>(product));
        }

        [Authorize]
        public async Task<ActionResult> GetUserProducts()
        {
            IEnumerable<Product> products =
                await _productService.Value.UserProducts(Guid.Parse(User.Identity.GetUserId()));

            return
                View(Mapper.Map<IEnumerable<Product>, IEnumerable<ProductViewModel>>(products.OrderBy(p => p.StartDate)));
        }

        [OutputCache(Duration = 50000, VaryByParam = "id", Location = OutputCacheLocation.ServerAndClient)]
        public async Task<ActionResult> GetImage(Guid id)
        {
            Product product = await _productService.Value.Get(id);

            if (product.ImageData == null)
            {
                var path = Server.MapPath("~/Content/img");
                var file = Directory.GetFiles(path, $"NoPhoto.*").FirstOrDefault();
                if (file == null)
                {
                    return null;
                }

                return File(Path.Combine(path, file), $"image/{Path.GetExtension(file)}");
            }

            byte[] imageBytes = Convert.FromBase64String(product.ImageData);
            return File(imageBytes, product.ImageType);
        }
    }
}