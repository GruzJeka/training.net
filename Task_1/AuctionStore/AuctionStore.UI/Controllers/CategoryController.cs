﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using AuctionStore.Business.Abstract;
using AuctionStore.Business.Entities;
using AuctionStore.Business.Infrastructure;
using AuctionStore.UI.Models.CategoriesViewModels;
using AutoMapper;
using NLog;
using Rsc;

namespace AuctionStore.UI.Controllers
{
    [HandleError(View = "ErrorView")]
    [Authorize(Roles = "Admin, Moderator")]
    public class CategoryController : Controller
    {
        private readonly Lazy<ICategoryService> _categoryService;

        private readonly Lazy<ILogger> _logger;

        public CategoryController(Lazy<ICategoryService> categoryService, Lazy<ILogger> logger)
        {
            _categoryService = categoryService;
            _logger = logger;
        }

        public ViewResult Create()
        {
            return View(new CategoryViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CategoryViewModel category)
        {
            if (ModelState.IsValid)
            {
                this._categoryService.Value.Make(Mapper.Map<CategoryViewModel, Category>(category));

                TempData["message"] = Resource.CategoryCreated;
                _logger.Value.Info($"{category.Name} created");
                return RedirectToAction("CategoriesManage", "Admin");
            }

            return View(category);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public RedirectToRouteResult Delete(Guid id, string returnUrl)
        {
            try
            {
                _categoryService.Value.Delete(id);
                TempData["message"] = Resource.CategoryDeleted;
            }
            catch (ValidationException)
            {
                TempData["error-message"] = Resource.RemoveAllProductsUnderCategory;
            }

            return RedirectToAction("CategoriesManage", "Admin");
        }

        public async Task<ViewResult> Update(Guid id)
        {
            return View(Mapper.Map<Category, CategoryViewModel>(await _categoryService.Value.Get(id)));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(CategoryViewModel category)
        {
            if (ModelState.IsValid)
            {
                this._categoryService.Value.Update(Mapper.Map<CategoryViewModel, Category>(category));
                TempData["message"] = Resource.CategoryUpdated;

                _logger.Value.Info($"{category.Name} updated");
                return RedirectToAction("CategoriesManage", "Admin");
            }

            return View("Create", category);
        }
    }
}