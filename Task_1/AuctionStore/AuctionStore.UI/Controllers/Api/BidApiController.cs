﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using AuctionStore.Business.Abstract;
using AuctionStore.Business.Entities;
using Microsoft.AspNet.Identity;

namespace AuctionStore.UI.Controllers.Api
{
    public class BidApiController : ApiController
    {
        private readonly IBidService _bidService;

        public BidApiController(IBidService bidService)
        {
            _bidService = bidService;
        }

        public async Task<HttpResponseMessage> Get()
        {
            string userId = User.Identity.GetUserId();
            if (userId != null)
            {
                IEnumerable<Bid> bids = await _bidService.UserBids(Guid.Parse(userId));
                return Request.CreateResponse(HttpStatusCode.OK, bids);
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        public HttpResponseMessage Post([FromBody] Guid productId)
        {
            try
            {
                string userId = User.Identity.GetUserId();
                if (userId != null)
                {
                    _bidService.Make(Guid.Parse(userId), productId);
                    return Request.CreateResponse(HttpStatusCode.Created);
                }

                return Request.CreateResponse(HttpStatusCode.BadRequest, "necessary get authetication cookie");
            }
            catch (Exception exception)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, exception.Message);
            }
        }
    }
}
