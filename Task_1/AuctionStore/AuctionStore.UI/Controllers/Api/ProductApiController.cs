﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using AuctionStore.Business.Abstract;
using AuctionStore.Business.Entities;
using AuctionStore.DAL.EnumsDto;
using Microsoft.AspNet.Identity;

namespace AuctionStore.UI.Controllers.Api
{
    public class ProductApiController : ApiController
    {
        private readonly IProductService _products;

        public ProductApiController(IProductService products)
        {
            _products = products;
        }

        public async Task<HttpResponseMessage> Get()
        {
            IEnumerable<Product> products = await _products.All();
            if (products.Count() != 0)
            {
                return Request.CreateResponse(HttpStatusCode.OK, products);
            }

            return Request.CreateResponse(HttpStatusCode.NotFound);
        }

        public async Task<HttpResponseMessage> GetByCategory(string category)
        {
            IEnumerable<Product> products = await _products.All(p => p.Category.Name == category);
            if (products.Count() != 0)
            {
                return Request.CreateResponse(HttpStatusCode.OK, products);
            }

            return Request.CreateResponse(HttpStatusCode.NotFound);
        }

        public async Task<HttpResponseMessage> GetProductById(string id)
        {
            Product product = await _products.Get(Guid.Parse(id));
            if (product != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, product);
            }

            return Request.CreateResponse(HttpStatusCode.NotFound);
        }


        public async Task<HttpResponseMessage> Post([FromBody] Product product)
        {
            try
            {
                product.State = ProductState.Selling;
                product.UserId = Guid.Parse(User.Identity.GetUserId());
                await _products.Create(product, product.UserId);
                return Request.CreateResponse(HttpStatusCode.Created);
            }
            catch (Exception exception)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, exception.Message);
            }
        }
    }
}
