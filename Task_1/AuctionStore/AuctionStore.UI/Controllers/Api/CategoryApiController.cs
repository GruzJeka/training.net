﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AuctionStore.Business.Abstract;
using AuctionStore.Business.Entities;

namespace AuctionStore.UI.Controllers.Api
{
    public class CategoryApiController : ApiController
    {
        private readonly ICategoryService _categoryService;

        public CategoryApiController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        public HttpResponseMessage GetCategories()
        {
            IEnumerable<Category> categories = _categoryService.All();
            if (categories.Count() != 0)
            {
                return Request.CreateResponse(HttpStatusCode.OK, categories);
            }

            return Request.CreateResponse(HttpStatusCode.NotFound);
        }

    }
}
