﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AuctionStore.Business.Abstract;

namespace AuctionStore.UI.Controllers.Api
{
    public class AuctionController : ApiController
    {
        private readonly IAuctionStore _houses;

        public AuctionController(IAuctionStore houses)
        {
            _houses = houses;
        }

        [Route("api/auctions")]
        public HttpResponseMessage GetAllAuctions()
        {
            var houses = _houses.GetAll().Select(h => h.Name);
            if (houses.Count() != 0)
            {
                return Request.CreateResponse(HttpStatusCode.OK, houses);
            }

            return Request.CreateResponse(HttpStatusCode.NotFound);
        }
    }
}
