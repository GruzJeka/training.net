﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using AuctionStore.Business.Entities;
using AuctionStore.UI.Models.UserViewModels;
using AuctionStore.UI.UserService;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;

namespace AuctionStore.UI.Controllers.Api
{
    public class AuthenticationApiController : ApiController
    {
        private readonly WcfUserServiceClient _userClinet;

        public AuthenticationApiController(WcfUserServiceClient userClinet)
        {
            _userClinet = userClinet;
        }

        protected IAuthenticationManager AuthenticationManager => HttpContext.Current.GetOwinContext().Authentication;

        [Route("api/Authentication/UserInfo")]
        public async Task<HttpResponseMessage> Get()
        {

            if (User.Identity.IsAuthenticated)
            {
                User user = await _userClinet.FindUserByNameAsync(User.Identity.Name);
                return Request.CreateResponse(HttpStatusCode.OK, user);
            }

            return Request.CreateResponse(HttpStatusCode.Unauthorized, false);
        }

        [Route("api/Authentication/Login")]
        public async Task<HttpResponseMessage> Post([FromBody]LoginViewModel model)
        {
            var user = await _userClinet.FindAsync(model.UserName, model.Password);

            if (user == null)
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized);
            }

            var identity = await _userClinet.GetClaimsIdentityAsync(user.Id.ToString());
            if (identity != null)
            {
                AuthenticationManager.SignIn(identity);
                return Request.CreateResponse(HttpStatusCode.OK, user);
            }

            return Request.CreateResponse(HttpStatusCode.Unauthorized);
        }

        [Route("api/Authentication/Logout")]
        public HttpResponseMessage Post()
        {
            if (User.Identity.IsAuthenticated)
            {
                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                return Request.CreateResponse(HttpStatusCode.OK);
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }
    }
}
