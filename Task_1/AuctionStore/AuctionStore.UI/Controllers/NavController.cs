﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AuctionStore.Business.Abstract;
using AuctionStore.UI.Models.EnumsViewModels;

namespace AuctionStore.UI.Controllers
{
    public class NavController : Controller
    {
        private readonly Lazy<ICategoryService> _categoryService;

        public NavController(Lazy<ICategoryService> categoryService)
        {
            _categoryService = categoryService;
        }

        public PartialViewResult CategoriesMenu(string category = null)
        {
            ViewBag.SelectedCategory = category;
            IEnumerable<string> categories =
                this._categoryService.Value.All().Select(c => c.Name).OrderBy(c => c).Distinct();
            return PartialView(categories);
        }

        public PartialViewResult ProductsMenu(ProductStateView? state)
        {
            ViewBag.SelectedProductState = state;
            ProductStateView[] productStates = (ProductStateView[])Enum.GetValues(typeof(ProductStateView));
            return PartialView("_ProductsMenu", productStates);
        }
    }
}