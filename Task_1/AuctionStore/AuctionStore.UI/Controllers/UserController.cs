﻿using System.Threading.Tasks;
using System.Web.Mvc;
using AuctionStore.Business.Entities;
using AuctionStore.Business.Helpers;
using AuctionStore.Business.Models;
using AuctionStore.DAL.EnumsDto;
using AuctionStore.UI.Helpers;
using AuctionStore.UI.Models.UserViewModels;
using AuctionStore.UI.UserService;
using AutoMapper;

namespace AuctionStore.UI.Controllers
{
    [Authorize(Roles = "Admin")]
    [HandleError(View = "ErrorView")]
    public class UserController : Controller
    {
        private readonly WcfUserServiceClient _userClient;

        public UserController(WcfUserServiceClient userClient)
        {
            _userClient = userClient;
        }

        public ViewResult Create()
        {
            return View(new CreateUserViewModel()
            {
                TimeZonesSelectListItems = TimeZoneHelper.GetTimeZonesSelectListItems()
            });
        }

        [HttpPost]
        public async Task<ActionResult> Create(CreateUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                User user = Mapper.Map<CreateUserViewModel, User>(model);
                OperationResult result = await _userClient.RegisterAsync(user);
                if (result.Success)
                {
                    TempData["message"] = $"Пользователь создан :{user.UserName}";
                    return RedirectToAction("UsersManage", "Admin");
                }

                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error);
                }
            }

            model.TimeZonesSelectListItems = TimeZoneHelper.GetTimeZonesSelectListItems();
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> AddToRole(string userId, Role role)
        {
            OperationResult result = await _userClient.AddToRoleAsync(userId, role);
            if (!result.Success)
            {
                TempData["error-message"] = result.Errors.ConcatAll();
            }

            return RedirectToAction("UsersManage", "Admin");
        }

        [HttpPost]
        public async Task<ActionResult> DeleteFromRole(string userId, Role role)
        {
            OperationResult result = await _userClient.RemoveFromRoleAsync(userId, role);
            if (!result.Success)
            {
                TempData["error-message"] = result.Errors.ConcatAll();
            }

            return RedirectToAction("UsersManage", "Admin");
        }

        [HttpPost]
        public async Task<ActionResult> DeleteUser(string id)
        {
            OperationResult result = await _userClient.DeleteUserAsync(id);
            if (result.Success)
            {
                TempData["message"] = "User deleted";
            }
            else
            {
                TempData["error-message"] = result.Errors.ConcatAll();
            }
           
            return RedirectToAction("UsersManage", "Admin");
        }
    }
}