﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace AuctionStore.UI.Controllers
{
    public class CultureController : Controller
    {
        public static List<string> Cultures => new List<string>()
        {
            "ru", "en", "be"
        };

        public ActionResult ChangeCulture(string lang, string returnUrl)
        {
            if (!Cultures.Contains(lang))
            {
                lang = "ru";
            }

            HttpCookie cookie = Request.Cookies["lang"];
            if (cookie != null)
            {
                cookie.Value = lang;
            }
            else
            {
                cookie = new HttpCookie("lang")
                {
                    HttpOnly = false,
                    Value = lang,
                    Expires = DateTime.Now.AddYears(1)
                };
            }

            Response.Cookies.Add(cookie);

            return RedirectToAction("ProductsList", "Product");
        }

        public PartialViewResult Languages()
        {
            return PartialView("_ListItems", Cultures);
        }
    }
}