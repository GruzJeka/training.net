﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Mvc;
using AuctionStore.Business.Abstract;
using AuctionStore.Business.Entities;
using AuctionStore.Business.Helpers;
using AuctionStore.Business.Models;
using AuctionStore.DAL.EnumsDto;
using AuctionStore.UI.Helpers;
using AuctionStore.UI.Models;
using AuctionStore.UI.Models.CategoriesViewModels;
using AuctionStore.UI.Models.ProductsViewModels;
using AuctionStore.UI.Models.UserViewModels;
using AuctionStore.UI.UserService;
using AutoMapper;
using Microsoft.AspNet.Identity;
using NLog;

namespace AuctionStore.UI.Controllers
{
    [HandleError(View = "ErrorView")]
    public class AdminController : Controller
    {
        private readonly Lazy<IProductService> _productService;

        private readonly Lazy<ICategoryService> _categoryService;

        private readonly Lazy<IAuctionStore> _houses;

        private readonly Lazy<ILogger> _logger;

        private readonly Lazy<WcfUserServiceClient> _userClient;

        public AdminController(Lazy<IProductService> productService, Lazy<ICategoryService> categoryService, Lazy<IAuctionStore> houses, Lazy<ILogger> logger, Lazy<WcfUserServiceClient> userClient)
        {
            _productService = productService;
            _categoryService = categoryService;
            _houses = houses;
            _logger = logger;
            _userClient = userClient;
        }

        [Authorize(Roles = "Moderator, Admin")]
        public async Task<ViewResult> ProductsManage(ProductState? state = null)
        {
            User user = await _userClient.Value.FindUserByIdAsync(User.Identity.GetUserId());
            ViewBag.TimeZoneId = user.TimeZoneId;

            IEnumerable<Product> products = await _productService.Value.All();

            if (User.IsInRole("Moderator"))
            {
                IEnumerable<string> categories = ModeratorCategories((string)RouteData.Values["auction"]);
                List<Product> productsList = new List<Product>();
                foreach (var category in categories)
                {
                    productsList.AddRange(products.Where(product => product.Category.Name == category));
                }

                products = productsList;
            }

            if (state.HasValue)
            {
                return
                    View(Mapper.Map<IEnumerable<Product>, IEnumerable<ProductViewModel>>(
                        products.Where(p => p.State == state.Value)));
            }

            return
                View(Mapper.Map<IEnumerable<Product>, IEnumerable<ProductViewModel>>(products).OrderBy(p => p.StartDate));
        }

        [Authorize(Roles = "Admin")]
        public ViewResult CategoriesManage()
        {
            return View(Mapper.Map<IEnumerable<Category>, IEnumerable<CategoryViewModel>>(
                        _categoryService.Value.All().OrderBy(u => u.Name)));
        }

        [Authorize(Roles = "Admin")]
        public ViewResult AuctionHousesManage()
        {
            return View(_houses.Value.GetAll().OrderBy(h => h.Name));
        }

        [Authorize(Roles = "Admin")]
        public async Task<ViewResult> UsersManage()
        {
            User user = await _userClient.Value.FindUserByIdAsync(User.Identity.GetUserId());
            ViewBag.TimeZoneId = user.TimeZoneId;

            return
                View(
                    Mapper.Map<IEnumerable<User>, IEnumerable<UserViewModel>>(
                        _userClient.Value.GetUsers().OrderBy(u => u.RegistrationDate).ToList()));
        }

        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> ModeratorClaims(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            IList<Claim> claims = await _userClient.Value.GetClaimsAsync(id);
            User user = await _userClient.Value.FindUserByIdAsync(id);

            return View(new ModeratorCategoriesManageViewModel()
            {
                Categories = SelectListItemHelper.CategoriesForModerator(_categoryService.Value.All(), claims, (string)RouteData.Values["auction"]),
                Claims = claims.Where(c => c.Type == ClaimType.CategoryManage),
                Name = user.UserName,
                Id = user.Id.ToString()
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> AddCategoryToManageModerator(string userId, string categoryName)
        {
           OperationResult result = await _userClient.Value.AddClaimAsync(userId, new Claim(ClaimType.CategoryManage, RouteData.Values["auction"] + "/" + categoryName));
            if (result.Success)
            {
                _logger.Value.Info($"Moderator category added for management. Category Name: {categoryName}");
            }
            else
            {
                TempData["error-message"] = result.Errors.ConcatAll();
            }
            
            return RedirectToRoute("route",
                new { auction = RouteData.Values["auction"], id = userId, action = "ModeratorClaims" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> DeleteCategoryFromManageModerator(string userId, string categoryName)
        {
            OperationResult result = await _userClient.Value.RemoveClaimAsync(userId, new Claim(ClaimType.CategoryManage, categoryName));
            if (result.Success)
            {
                _logger.Value.Info($"moderator delete a category management. Category Name : {categoryName}");
            }
            else
            {
                TempData["error-message"] = result.Errors.ConcatAll();
            }

            return RedirectToRoute("route",
                new { auction = RouteData.Values["auction"], id = userId, action = "ModeratorClaims" });
        }

        private IEnumerable<string> ModeratorCategories(string auction)
        {
            var claimsIdentity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims =
                claimsIdentity.Claims.Where(c => c.Type == ClaimType.CategoryManage && c.Value.Contains(auction));

            IEnumerable<string> categories = claims.Select(c => c.Value.Split('/')[1]);
            return categories.ToList();
        }
    }
}
