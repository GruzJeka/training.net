﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AuctionStore.Business.Entities;
using AuctionStore.Business.Helpers;
using AuctionStore.Business.Models;
using AuctionStore.UI.Helpers;
using AuctionStore.UI.Models.UserViewModels;
using AuctionStore.UI.UserService;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using NLog;
using Rsc;

namespace AuctionStore.UI.Controllers
{
    [HandleError(View = "ErrorView")]
    public class AccountController : Controller
    {
        private readonly Lazy<ILogger> _logger;

        private readonly Lazy<WcfUserServiceClient> _userClient;

        public AccountController(Lazy<ILogger> logger, Lazy<WcfUserServiceClient> userClient)
        {
            _logger = logger;
            _userClient = userClient;
        }

        protected IAuthenticationManager AuthenticationManager => HttpContext.GetOwinContext().Authentication;

        public ActionResult Login()
        {
            return View(new LoginViewModel());
        }

        [HttpPost]
        public async Task<ActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                User user = await _userClient.Value.FindAsync(model.UserName, model.Password);
                if (user != null)
                {
                    ClaimsIdentity identity = await _userClient.Value.GetClaimsIdentityAsync(user.Id.ToString());
                    AuthenticationManager.SignIn(identity);

                    _logger.Value.Info($"{model.UserName} sign in");
                    return RedirectToAction("ProductsList", "Product");
                }

                ModelState.AddModelError(string.Empty, Resource.WrongLoginOrPassword);
            }

            return View(model);
        }

        [Authorize]
        public ActionResult Logout()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

            _logger.Value.Info($"{User.Identity.Name} sign out");
            return RedirectToAction("ProductsList", "Product");
        }

        public ActionResult Register()
        {
            return View(new CreateUserViewModel()
            {
                TimeZonesSelectListItems = TimeZoneHelper.GetTimeZonesSelectListItems()
            });
        }

        [HttpPost]
        public async Task<ActionResult> Register(CreateUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                OperationResult result = await _userClient.Value.RegisterAsync(Mapper.Map<CreateUserViewModel, User>(model));

                if (result.Success)
                {
                    User user = await _userClient.Value.FindUserByNameAsync(model.UserName);
                    ClaimsIdentity identity = await _userClient.Value.GetClaimsIdentityAsync(user.Id.ToString());
                    AuthenticationManager.SignIn(identity);

                    _logger.Value.Info($"{User.Identity.Name} register");
                    return RedirectToAction("ProductsList", "Product");
                }

                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error);
                }
            }

            model.TimeZonesSelectListItems = TimeZoneHelper.GetTimeZonesSelectListItems();
            return View(model);
        }

        [Authorize]
        public async Task<ActionResult> UserProfile()
        {
            User user = await _userClient.Value.FindUserByIdAsync(User.Identity.GetUserId());
            ViewBag.TimeZonesSelectListItems = TimeZoneHelper.GetTimeZonesSelectListItems();

            return View(Mapper.Map<User, UserProfileViewModel>(user));
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> UserProfile(UserProfileViewModel model)
        {
            UserProfileModel profile = Mapper.Map<UserProfileViewModel, UserProfileModel>(model);
            profile.Id = User.Identity.GetUserId();

            OperationResult result = await _userClient.Value.UpdateUserProfileAsync(profile);

            if (result.Success)
            {
                TempData["message"] = Resource.ProfileUpdated;
                _logger.Value.Info($"{User.Identity.Name} updated their profile");
            }
            else
            {
                TempData["error-message"] = result.Errors.ConcatAll();
            }

            return RedirectToAction("UserProfile");
        }

        [HttpPost]
        public async Task<ActionResult> ChangePassword(UserProfileViewModel model)
        {
            string userId = User.Identity.GetUserId();
            OperationResult result =
                await _userClient.Value.ChangePasswordAsync(userId, model.ChangePasswordModel.Password);

            if (result.Success)
            {
                TempData["message"] = Resource.ProfileUpdated;
                _logger.Value.Info($"{User.Identity.Name} change password");
                return RedirectToAction("UserProfile");
            }

            TempData["error-message"] = result.Errors.ConcatAll();
            return View("UserProfile");
        }
    }
}