﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using AuctionStore.Business.Abstract;
using AuctionStore.Business.Entities;
using AuctionStore.UI.Models.BidViewModels;
using AutoMapper;
using Microsoft.AspNet.Identity;
using NLog;
using Rsc;

namespace AuctionStore.UI.Controllers
{
    [HandleError(View = "ErrorView")]
    [Authorize(Roles = "User, Admin")]
    public class BidController : Controller
    {
        private readonly Lazy<IBidService> _bidService;

        private readonly Lazy<ILogger> _logger;

        public BidController(Lazy<IBidService> bidService, Lazy<ILogger> logger)
        {
            _bidService = bidService;
            _logger = logger;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<RedirectToRouteResult> Create(Guid productId)
        {
            await _bidService.Value.Make(Guid.Parse(User.Identity.GetUserId()), productId);

            TempData["message"] = Resource.BidCreated;
            _logger.Value.Info($"{User.Identity.Name} created bid");
            return RedirectToAction("UserBids", "Bid");
        }

        public async Task<ViewResult> UserBids(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            IEnumerable<Bid> bids = await _bidService.Value.UserBids(Guid.Parse(User.Identity.GetUserId()));
            return View(Mapper.Map<IEnumerable<Bid>, IEnumerable<BidViewModel>>(bids));
        }
    }
}