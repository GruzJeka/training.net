﻿using System;
using System.Linq;
using System.Web.Mvc;
using AuctionStore.Business.Abstract;
using AuctionStore.Business.Entities;
using AuctionStore.Business.Infrastructure;
using AuctionStore.UI.Models;
using AutoMapper;
using NLog;

namespace AuctionStore.UI.Controllers
{
    [HandleError(View = "ErrorView")]
    public class AuctionHouseController : Controller
    {
        private readonly Lazy<IAuctionStore> _houses;

        private readonly Lazy<ILogger> _logger;

        private IConnectionStore _connectionStore;

        public AuctionHouseController(Lazy<IAuctionStore> houses, Lazy<ILogger> logger)
        {
            _houses = houses;
            _logger = logger;
        }

        public ViewResult Create()
        {
            return View(new AuctionHouseViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AuctionHouseViewModel house)
        {
            if (ModelState.IsValid)
            {
                _houses.Value.Create(Mapper.Map<AuctionHouseViewModel, AuctionHouse>(house));
                _connectionStore = new ConnectionStore(house.Type, _logger);
                _connectionStore.AddConnection(house.Location);

                _logger.Value.Info($"created auction house : {house.Name}");
                return RedirectToAction("AuctionHousesManage", "Admin");
            }

            return View(house);
        }

        public ViewResult Update(string id)
        {
            return View(Mapper.Map<AuctionHouse, AuctionHouseUpdateViewModel>(_houses.Value.Get(id)));
        }

        [HttpPost]
        public ActionResult Update(AuctionHouseUpdateViewModel house, string oldHouseName)
        {
            if (ModelState.IsValid)
            {
                AuctionHouse newHouse = Mapper.Map<AuctionHouseUpdateViewModel, AuctionHouse>(house);
                _houses.Value.Update(oldHouseName, newHouse);

                _logger.Value.Info($"updated auction house : {house.Name}");
                TempData["message"] = "Auction house updated";

                return RedirectToRoute(new
                {
                    controller = "Admin",
                    action = "AuctionHousesManage",
                    auction = newHouse.Name
                });
            }

            return View(house);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(string name)
        {
            if (_houses.Value.GetAll().Count() == 1)
            {
                TempData["error-message"] = "before you remove the need to create new house";
                return RedirectToAction("AuctionHousesManage", "Admin");
            }

            AuctionHouse house = _houses.Value.Get(name);

            if (house != null)
            {
                _houses.Value.Delete(house.Name);
                _connectionStore = new ConnectionStore(house.Type, _logger);
                _connectionStore.RemoveConnection(house.Location);
            }
            else
            {
                TempData["message"] = $"Not found Auction house : {name}";
            }

            _logger.Value.Info($"deleted auction house : {name}");
            return RedirectToAction("AuctionHousesManage", "Admin");
        }

        public PartialViewResult GetHouses()
        {
            return PartialView("_ListItems", _houses.Value.GetAll().Select(h => h.Name));
        }

        [HttpPost]
        public ActionResult ChangeHouse(string name)
        {
            string url = Url.RouteUrl(new
            {
                controller = "Product",
                action = "ProductsList",
                category = (string)null,
                page = 1,
                auction = name
            });

            if (Request.IsAjaxRequest())
            {
                return Json(new { newurl = url });
            }

            return Redirect(url);
        }
    }
}