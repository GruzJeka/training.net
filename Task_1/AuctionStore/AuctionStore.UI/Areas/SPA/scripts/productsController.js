﻿var module = angular.module('auctionApp');

module.controller('ProductController',
    function($scope, auctionFactory, productFactory, $stateParams, categoryFactory,$state) {

        $scope.status;
        $scope.products;
        $scope.categories= {};
        $scope.item = {}

        $scope.createProduct = function() {
            $state.go("root.createProduct", { auctionHouse: $stateParams.auctionHouse});
        }

        $scope.makeBid = function(productId) {
            
        }

        getAllCategories();


        function getAllCategories() {
            categoryFactory.getCategories($stateParams.auctionHouse)
                .then(function (response) {
                    $scope.categories = response.data;
                    console.log(response.data);
                }, function (error) {
                    $scope.status = error.message;
                });
        }

        function getProducts() {
            if (angular.isUndefined($stateParams.category)) {
                productFactory.getProducts($stateParams.auctionHouse)
                    .then(function(response) {
                        $scope.products = response.data;
                        },
                        function(error) {
                            $scope.status = error.message;
                        });
            } else {
                productFactory.getProductByCategory($stateParams.auctionHouse, $stateParams.category)
                   .then(function (response) {
                       $scope.products = response.data;
                   },
                       function (error) {
                           $scope.status = error.message;
                       });
            }
        }

        getProducts();
      
    });