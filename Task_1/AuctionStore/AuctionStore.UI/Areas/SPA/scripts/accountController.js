﻿angular.module('auctionApp')
    .controller('AccountController',
        function($scope, $stateParams, accountFactory, $state, $rootScope) {

            $scope.userName;
            $scope.password;

            $scope.signIn = function() {
                var loginModel = {
                    UserName: $scope.userName,
                    Password: $scope.password
                };

                accountFactory.login(loginModel)
                    .then(function(response) {
                            if (!angular.isUndefined(response.data.UserName)) {
                                $rootScope.user = response.data;
                                $rootScope.authentication.isAuthenticated = true;
                                $state.go('root');
                            } else {
                                $scope.message = "Неверный логин или пароль";
                                $state.go('root.account');
                            }
                        },
                        function(error) {

                        });
            }
        });