﻿var module = angular.module('auctionApp', ['ui.router', 'ngResource', 'ngMessages']);
module.config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('root',
        {
            url: '/',
            views: {
                "header": {
                    templateUrl: '/static/header.html'
                },
                "content": {
                    templateUrl: '/static/content.html'
                },
                "footer": {
                    templateUrl: '/static/footer.html'
                }
            }
        })
        .state('root.products',
        {
            url: 'Products/:auctionHouse',
            views: {
                "content@": {
                    templateUrl: '/static/products.html'
                }
            }
        })
        .state('root.products.categories',
        {
            url: '/:category',
            views: {
                "content@": {
                    templateUrl: '/static/products.html'
                }
            }
        })
        .state('root.products.categories.details',
        {
            url: '/:id',
            views: {
                "content@": {
                    templateUrl: '/static/details.html'
                }
            }
        })
        .state('root.createProduct',
        {
            url: 'CreateProduct',
            views: {
                "content@": {
                    templateUrl: '/static/editProduct.html'
                }
            }
        })
        .state('root.account',
        {
            url: '/SignIn',
            views: {
                "content@": {
                    templateUrl: '/static/account.html'
                }
            }
        })
        .state('root.bids',
        {
            url: ':auctionHouse/Bids',
            views: {
                "content@": {
                    templateUrl: '/static/bids.html'
                }
            }
        });

}).run(function($rootScope, accountFactory) {
    function checkIsUserAuthenticated() {
        accountFactory.isAuthenticated()
            .then(function (response) {
                if (response.status == '200') {
                    $rootScope.authentication.isAuthenticated = true;
                    $rootScope.user = response.data;
                } else {
                    $rootScope.authentication.isAuthenticated = false;
                    $rootScope.user = null;
                }
            },
                function (error) {
                });
    }

    checkIsUserAuthenticated();
});
