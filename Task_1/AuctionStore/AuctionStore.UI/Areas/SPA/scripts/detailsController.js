﻿angular.module('auctionApp')
    .controller('DetailsController', function ($scope, $stateParams, productFactory, bidFactory, $state) {
        $scope.item = {}

        $scope.makeBid=function(productId) {
            bidFactory.createBid($stateParams.auctionHouse, productId)
                .then(function (response) {
                    $state.go('root.bids');
                }, function (error) {

            });
        }

        function getProduct() {
            productFactory.getProductById($stateParams.auctionHouse, $stateParams.id)
                .then(function (response) {
                    console.log(response.data);
                    $scope.item = response.data;
                }, function(error) {
                    $scope.status = error.message;
                });
        }

        getProduct();
    });