﻿angular.module('auctionApp')
    .controller('NavigationController', function ($scope, auctionFactory, $state, accountFactory, $rootScope) {

        $scope.auctions;
        $scope.currentAuction;
        $scope.status;

        $rootScope.user = {};
        $rootScope.authentication = { isAuthenticated: true };

        $scope.changeAuction = function (auction) {
            $scope.currenAuction = auction;
            $state.go('root.products', { auctionHouse: auction });
        }

        $scope.signIn = function () {
            $state.go("root.account");
        }

        $scope.signOut = function() {
            accountFactory.logout()
                .then(function(response) {
                        if (response.status == '200') {
                            $rootScope.authentication.isAuthenticated = false;
                            $rootScope.user = null;
                            console.log('logout');
                        }
                    },
                    function(error) {
                        $scope.status = error.message;
                    });
        }

        function getAuctions() {
            auctionFactory.getAuctions()
                .then(function (response) {
                    $scope.auctions = response.data;
                },
                    function (error) {
                        $scope.status = error.message;
                    });
        }



        getAuctions();
    });