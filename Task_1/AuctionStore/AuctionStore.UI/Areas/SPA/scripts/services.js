﻿angular.module('auctionApp')
    .factory('productFactory',
        function($http) {
            var productFactory = {}

            var baseUrl = '/api/ProductApi/';

            productFactory.getProducts = function(auction) {
                return $http.get(baseUrl + auction + 'Auction');
            }

            productFactory.getProductById = function(auction, id) {
                return $http.get(baseUrl + auction + 'Auction/' + 'id' + id);
            }

            productFactory.getProductByCategory= function(auction, category) {
                return $http.get(baseUrl + auction + 'Auction/' + category);
            }

            productFactory.createProduct = function (auction, product) {
                console.log(baseUrl + auction + 'Auction');
                return $http.post(baseUrl + auction + 'Auction', product);
            }

            return productFactory;
        });

angular.module('auctionApp')
    .factory('auctionFactory',
        function($http) {
            var baseUrl = '/api/auctions';

            var auctionFactory = {}

            auctionFactory.getAuctions = function() {
                return $http.get(baseUrl);
            }

            return auctionFactory;
        });

angular.module('auctionApp')
    .factory('categoryFactory', function($http) {
        var baseUrl = '/api/CategoryApi/';

        var categoryFactory = {}

        categoryFactory.getCategories = function(auction) {
            return $http.get(baseUrl + auction + 'Auction');
        }

        return categoryFactory;
    });

angular.module('auctionApp')
    .factory('accountFactory', function($http) {
        var baseUrl = '/api/Authentication';

        var accountFactory = {}

        accountFactory.logout= function() {
            return $http.post(baseUrl + '/Logout');
        }

        accountFactory.login = function(loginModel) {
           return  $http.post(baseUrl + '/Login', loginModel);
        }

        accountFactory.isAuthenticated = function() {
            return $http.get(baseUrl + '/UserInfo');
        }

        return accountFactory;

    });

angular.module('auctionApp')
    .factory('bidFactory',
        function($http) {
            var baseUrl = '/api/BidApi';

            var bidFactory = {};

            bidFactory.createBid = function(auction) {
                return $http.post(baseUrl + '/' + auction + 'Auction');
            }

            bidFactory.getBids = function(auction) {
                return $http.get(baseUrl + '/' + auction + 'Auction');
            }

            return bidFactory;
        });