﻿angular.module('auctionApp')
    .controller('EditProductController', function ($scope, productFactory, $stateParams, $state, categoryFactory) {

        $scope.name;
        $scope.description;
        $scope.duration;
        $scope.startPrice;
        $scope.categoryId;

        $scope.categories={}
        $scope.showCategory = false;

        $scope.changeHouse = function () {
            categoryFactory.getCategories($scope.auction).then(function (response) {
                $scope.categories = response.data;
                $scope.showCategory = true;
            }, function (error) {

            });
       }

        $scope.editProduct = function(isValid) {
            if (isValid) {
                var product = {
                    Name :$scope.name,
                    Description: $scope.description,
                    Duration: $scope.duration,
                    StartPrice:$scope.StartPrice,
                    CategoryId:$scope.categoryId
                };

                productFactory.createProduct($scope.auction, product)
                    .then(function (response) {
                            $state.go("root");
                        },
                    function (error) {
                        $scope.status = error.message;
                    });
            } 
        }
    });