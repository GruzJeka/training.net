﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AuctionStore.Business.Abstract;
using AuctionStore.Business.Entities;
using AuctionStore.Business.Enums;
using AuctionStore.Business.Infrastructure;
using AuctionStore.Business.Services;
using AuctionStore.DAL.Abstract;
using AuctionStore.DAL.Context;
using AuctionStore.DAL.Repositories;
using Moq;
using NLog;
using NUnit.Framework;

namespace AuctionStore.BusinessTests
{
    //[TestFixture]
    //public class BidServiceTests
    //{
    //    private readonly Product _actualProduct = new Product()
    //    {
    //        Name = "Product1",
    //        State = ProductState.Selling,
    //        Duration = TimeSpan.FromDays(30),
    //        StartDate = DateTime.Now,
    //        UserId = Guid.NewGuid(),
    //        StartPrice = 30
    //    };

    //    private Product _noActualProduct = new Product()
    //    {
    //        Name = "Product1",
    //        State = ProductState.Sold,
    //        Duration = TimeSpan.FromDays(30),
    //        StartDate = DateTime.Now,
    //        UserId = Guid.NewGuid(),
    //        StartPrice = 40
    //    };

    //    private Mock<IRepository<Product>> _productsMock;

    //    private Mock<IRepository<Bid>> _bidsMock;

    //    private Mock<IBidPriceCalculator> _calcMock;

    //    private Mock<ILogger> _loggerMock;

    //    private IBidService _bidService;

    //    [SetUp]
    //    public void Init()
    //    {
    //        _productsMock = new Mock<IRepository<Product>>();
    //        _bidsMock = new Mock<IRepository<Bid>>();
    //        _calcMock = new Mock<IBidPriceCalculator>();
    //        _loggerMock = new Mock<ILogger>();

    //        _bidService = new BidService(new Lazy<IRepository<Product>>(() => _productsMock.Object),
    //            new Lazy<IRepository<Bid>>(() => _bidsMock.Object),
    //            new Lazy<IBidPriceCalculator>(() => _calcMock.Object),
    //            new Lazy<ILogger>(() => _loggerMock.Object));

    //        _calcMock.Setup(m => m.Calculate(It.IsAny<decimal>(), It.IsAny<int>())).Returns(It.IsAny<decimal>());
    //        _bidsMock.Setup(m => m.GetAll()).Returns(new List<Bid>());

    //    }

    //    [Test]
    //    public void Can_Make_Bid()
    //    {
    //        _productsMock.Setup(m => m.Get(It.IsAny<Guid>())).Returns(_actualProduct);

    //        _bidService.Make(Guid.NewGuid(), Guid.NewGuid());

    //        _bidsMock.Verify(m => m.Create(It.IsAny<Bid>()));
    //    }

    //    [Test]
    //    public void Can_Make_Bid_NotValidProduct()

    //    {
    //        _productsMock.Setup(m => m.Get(It.IsAny<Guid>())).Returns(_noActualProduct);

    //        _bidService.Make(Guid.NewGuid(), Guid.NewGuid());

    //        _bidsMock.Verify(m => m.Create(It.IsAny<Bid>()));
    //    }
    //}
}
