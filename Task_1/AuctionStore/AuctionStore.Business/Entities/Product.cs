﻿using System;
using AuctionStore.DAL.Abstract;
using AuctionStore.DAL.EnumsDto;

namespace AuctionStore.Business.Entities
{
    public class Product : IEntity
    {
        private decimal _startPrice;

        private TimeSpan _duration;

        private ProductState _state;

        public Guid Id { get; set; }

        public Guid CategoryId { get; set; }

        public Category Category { get; set; }

        public Guid UserId { get; set; }

        public User User { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public ProductState State
        {
            get
            {
                if (this._state == ProductState.Selling && GetTimeLeft() <= TimeSpan.Zero)
                {
                    if (this.BidsQuantity == 0)
                    {
                        return ProductState.Draft;
                    }
                    else
                    {
                        return ProductState.Sold;
                    }
                }
                else
                {
                    return this._state;
                }
            }

            set
            {
                this._state = value;
            }
        }

        public DateTime StartDate { get; set; }

        public decimal StartPrice
        {
            get
            {
                return this._startPrice;
            }

            set
            {
                if (value < 0)
                {
                    throw new ArgumentException("Incorrect start price value");
                }

                this._startPrice = value;
            }
        }

        public TimeSpan Duration
        {
            get
            {
                return this._duration;
            }

            set
            {
                if (value < TimeSpan.Zero)
                {
                    throw new ArgumentException("Incorrect time span value");
                }

                this._duration = value;
            }
        }

        public int BidsQuantity { get; set; }

        public decimal BidPrice { get; set; }

        public string ImageData { get; set; }

        public string ImageType { get; set; }

        public TimeSpan GetTimeLeft()
        {
            DateTime finisDateTime = this.StartDate.Add(this.Duration);
            if (finisDateTime.Subtract(DateTime.Now) > TimeSpan.Zero)
            {
                return finisDateTime.Subtract(DateTime.Now);
            }

            return TimeSpan.Zero;
        }

        public bool IsActual()
        {
            if (this.GetTimeLeft() > TimeSpan.Zero && this.State == ProductState.Selling)
            {
                return true;
            }

            return false;
        }
    }
}
