﻿using System;
using AuctionStore.DAL.Abstract;
using AuctionStore.DAL.EnumsDto;

namespace AuctionStore.Business.Entities
{
    public class UserRole : IEntity
    {
        public Guid Id { get; set; }

        public Guid UserId { get; set; }

        public Role Role { get; set; }
    }
}
