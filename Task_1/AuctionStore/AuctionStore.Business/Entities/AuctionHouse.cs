﻿using System;
using AuctionStore.Business.Enums;

namespace AuctionStore.Business.Entities
{
    public class AuctionHouse
    {
        public AuctionHouse(string name, string location, DataType type)
        {
            Name = name;
            Location = location;
            Type = type;
        }

        public AuctionHouse()
        {
        }

        public string Name { get; set; }

        public string Location { get; set; }

        public DataType Type { get; set; }
    }
}
