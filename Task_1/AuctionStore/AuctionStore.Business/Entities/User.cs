﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using AuctionStore.DAL.Abstract;
using Microsoft.AspNet.Identity;

namespace AuctionStore.Business.Entities
{
    [DataContract]
    public class User : IUser<string>, IEntity
    {
        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public DateTime RegistrationDate { get; set; }

        [DataMember]
        public string TimeZoneId { get; set; }

        [DataMember]
        public IEnumerable<UserRole> UserRoles { get; set; }

        [DataMember]
        public IEnumerable<UserClaim> Claims { get; set; } 

        string IUser<string>.Id => Id.ToString();
    }
}
