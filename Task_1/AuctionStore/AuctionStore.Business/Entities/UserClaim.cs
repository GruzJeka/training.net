﻿using System;

namespace AuctionStore.Business.Entities
{
    public class UserClaim
    {
        public Guid Id { get; set; }

        public Guid UserId { get; set; }

        public User User { get; set; }

        public string Type { get; set; }

        public string Value { get; set; }
    }
}
