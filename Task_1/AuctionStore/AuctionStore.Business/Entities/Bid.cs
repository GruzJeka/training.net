﻿using System;
using AuctionStore.DAL.Abstract;
using AuctionStore.DAL.EnumsDto;

namespace AuctionStore.Business.Entities
{
    public class Bid : IEntity
    {
        private decimal _price;

        public Guid Id { get; set; }

        public DateTime DateTime { get; set; }

        public Guid UserId { get; set; }

        public Guid ProductId { get; set; }

        public Product Product { get; set; }

        public decimal Price
        {
            get
            {
                return this._price;
            }

            set
            {
                if (value < 0)
                {
                    throw new ArgumentException("Incorrect price value");
                }

                this._price = value;
            }
        }

        public BidState State { get; set; }
    }
}
