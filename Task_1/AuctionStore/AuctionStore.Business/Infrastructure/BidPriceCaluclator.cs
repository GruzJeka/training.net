﻿using AuctionStore.Business.Abstract;

namespace AuctionStore.Business.Infrastructure
{
    public class BidPriceCaluclator : IBidPriceCalculator
    {
        public BidPriceCaluclator(decimal percent)
        {
            Percent = percent;
        }

        public decimal Percent { get; }

        public decimal Calculate(decimal startPrice, int bidsQuantity)
        {
            decimal price = startPrice;

            for (int i = 1; i <= bidsQuantity; i++)
            {
                decimal percentPrice = price * Percent / 100;
                price = price + percentPrice;
            }

            return price;
        }
    }
}
