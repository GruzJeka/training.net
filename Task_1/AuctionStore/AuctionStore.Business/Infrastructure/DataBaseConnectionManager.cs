﻿using System;
using System.Configuration;
using System.Web.Configuration;
using AuctionStore.Business.Abstract;

namespace AuctionStore.Business.Infrastructure
{
    public class DataBaseConnectionManager : IConnectionManager
    {
        public void Add(string location)
        {
            try
            {
                var configuration = WebConfigurationManager.OpenWebConfiguration("~");
                var section = (ConnectionStringsSection)configuration.GetSection("connectionStrings");
                string connectionString =
                    $@"Data Source=(localdb)\ProjectsV12;AttachDBFilename=|DataDirectory|\{location}.mdf;Initial Catalog={location};Integrated Security=SSPI";
                section.ConnectionStrings.Add(new ConnectionStringSettings(location, connectionString, "System.Data.SqlClient"));
                configuration.Save();
            }
            catch (ConfigurationErrorsException errors)
            {
                throw new Exception("add database connection error", errors.InnerException);
            }
        }

        public void Remove(string location)
        {
            try
            {
                var configuration = WebConfigurationManager.OpenWebConfiguration("~");
                var section = (ConnectionStringsSection)configuration.GetSection("connectionStrings");
                section.ConnectionStrings.Remove(location);
                configuration.Save();
            }
            catch (ConfigurationErrorsException errors)
            {
                throw new Exception("remove database connection error", errors.InnerException);
            }
        }
    }
}
