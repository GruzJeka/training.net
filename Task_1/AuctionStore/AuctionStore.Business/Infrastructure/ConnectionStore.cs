﻿using System;
using AuctionStore.Business.Abstract;
using AuctionStore.Business.Enums;
using NLog;

namespace AuctionStore.Business.Infrastructure
{
    public class ConnectionStore : IConnectionStore
    {
        private readonly Lazy<ILogger> _logger;

        private IConnectionManager _connectionManager;

        public ConnectionStore(DataType type, Lazy<ILogger> logger)
        {
            Type = type;
            _logger = logger;
        }

        public DataType Type
        {
            set
            {
                switch (value)
                {
                    case DataType.Json:
                    {
                        _connectionManager = new FileConnectionManager();
                        break;
                    }

                    case DataType.Sql:
                    {
                        _connectionManager = new DataBaseConnectionManager();
                        break;
                    }
                }
            }
        }

        public void AddConnection(string location)
        {
            try
            {
                _connectionManager.Add(location);
            }
            catch (Exception exception)
            {
                _logger.Value.Error(exception.InnerException, exception.Message);
                throw;
            }
        }

        public void RemoveConnection(string location)
        {
            try
            {
                _connectionManager.Remove(location);
            }
            catch (Exception exception)
            {
                _logger.Value.Error(exception.InnerException, exception.Message);
                throw;
            }
        }
    }
}
