﻿using System;
using System.IO;
using System.Web.Hosting;
using AuctionStore.Business.Abstract;

namespace AuctionStore.Business.Infrastructure
{
    public class FileConnectionManager : IConnectionManager
    {
        public void Add(string location)
        {
            try
            {
                string path = HostingEnvironment.MapPath(@"~/App_Data/" + location + "/");
                if (path != null)
                {
                    Directory.CreateDirectory(path);
                }
            }
            catch (IOException exception)
            {
                throw new Exception("add file connection error", exception.InnerException);
            }
        }

        public void Remove(string location)
        {
            try
            {
                string path = HostingEnvironment.MapPath(@"~/App_Data/" + location + "/");
                if (path != null)
                {
                    Directory.Delete(path, true);
                }
            }
            catch (IOException exception)
            {
                throw new Exception("remove file connection error", exception.InnerException);
            }
        }
    }
}
