﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Configuration;
using AuctionStore.Business.Abstract;
using AuctionStore.Business.Entities;
using AuctionStore.DAL.ConfigEntities;
using AutoMapper;
using NLog;

namespace AuctionStore.Business.Repositories
{
    public class AuctionHousesWebConfigStore : IAuctionStore
    {
        private const string SectionName = "auctionHouses";

        private readonly ILogger _logger;

        private readonly Configuration _config;

        public AuctionHousesWebConfigStore(ILogger logger)
        {
            _logger = logger;
            _config = WebConfigurationManager.OpenWebConfiguration("~/");
            CreateSectionIfNotExists();
        }

        public IEnumerable<AuctionHouse> GetAll()
        {
            AuctionHousesSection section = _config.GetSection(SectionName) as AuctionHousesSection;

            IEnumerable<ConfigAuctionHouse> houses = section?.AuctionHouses.Cast<ConfigAuctionHouse>();
            return Mapper.Map<IEnumerable<ConfigAuctionHouse>, IEnumerable<AuctionHouse>>(houses);
        }

        public void Create(AuctionHouse house)
        {
            try
            {
                Configuration config = WebConfigurationManager.OpenWebConfiguration("~/");
                AuctionHousesSection section = config.GetSection(SectionName) as AuctionHousesSection;

                section.AuctionHouses.Add(Mapper.Map<AuctionHouse, ConfigAuctionHouse>(house));
                section.SectionInformation.ForceSave = true;
                config.Save(ConfigurationSaveMode.Modified);
            }
            catch (ConfigurationErrorsException errors)
            {
                _logger.Error(errors.Message);
                throw;
            }
        }

        public AuctionHouse Get(string houseName)
        {
            return this.GetAll().SingleOrDefault(p => p.Name == houseName);
        }

        public void Update(string oldHouseName, AuctionHouse newHouse)
        {
            AuctionHouse oldHouse = GetAll().FirstOrDefault(o => o.Name == oldHouseName);
            if (oldHouse != null)
            {
                newHouse.Location = oldHouse.Location;
                newHouse.Type = oldHouse.Type;
                this.Create(newHouse);
                this.Delete(oldHouseName);
            }
        }

        public void Delete(string houseName)
        {
            try
            {
                Configuration config = WebConfigurationManager.OpenWebConfiguration("~/");
                AuctionHousesSection section = config.GetSection(SectionName) as AuctionHousesSection;

                section.AuctionHouses.Remove(houseName);
                section.SectionInformation.ForceSave = true;
                config.Save(ConfigurationSaveMode.Modified);
            }
            catch (ConfigurationErrorsException errors)
            {
                _logger.Error(errors.Message);
                throw;
            }
        }

        private void CreateSectionIfNotExists()
        {
            try
            {
                AuctionHousesSection section = (AuctionHousesSection)_config.Sections[SectionName];
                if (section == null)
                {
                    section = new AuctionHousesSection();
                    _config.Sections.Add(SectionName, section);
                    _config.Save(ConfigurationSaveMode.Modified);
                }
            }
            catch (ConfigurationErrorsException errors)
            {
                _logger.Error(errors.Message);
                throw;
            }
        }
    }
}
