﻿namespace AuctionStore.Business.Abstract
{
    public interface IBidPriceCalculator
    {
        decimal Calculate(decimal startPrice, int bidsQuantity);
    }
}
