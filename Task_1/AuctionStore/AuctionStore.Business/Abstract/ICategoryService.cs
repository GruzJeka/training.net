﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AuctionStore.Business.Entities;

namespace AuctionStore.Business.Abstract
{
    public interface ICategoryService
    {
        Task Make(Category category);

        Task Update(Category category);

        Task<Category> Get(Guid id);

        Task<Category> Get(string name);

        Task Delete(Guid id);

        IEnumerable<Category> All();
    }
}
