﻿namespace AuctionStore.Business.Abstract
{
    public interface IConnectionStore
    {
        void AddConnection(string location);

        void RemoveConnection(string location);
    }
}
