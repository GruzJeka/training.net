﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AuctionStore.Business.Entities;

namespace AuctionStore.Business.Abstract
{
    public interface IBidService
    {
        Task Make(Guid userId, Guid productId);

        Task<Bid> Get(Guid id);

        Task<IEnumerable<Bid>> UserBids(Guid userId);
    }
}
