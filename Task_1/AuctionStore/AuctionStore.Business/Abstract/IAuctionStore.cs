﻿using System.Collections.Generic;
using AuctionStore.Business.Entities;

namespace AuctionStore.Business.Abstract
{
    public interface IAuctionStore
    {
        IEnumerable<AuctionHouse> GetAll();

        void Create(AuctionHouse house);

        AuctionHouse Get(string houseName);

        void Update(string oldHouseName, AuctionHouse newHouse);

        void Delete(string houseName);
    }
}
