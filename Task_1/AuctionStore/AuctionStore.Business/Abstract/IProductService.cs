﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AuctionStore.Business.Entities;
using AuctionStore.DAL.DtoEntities;
using AuctionStore.DAL.EnumsDto;

namespace AuctionStore.Business.Abstract
{
    public interface IProductService : IDisposable
    {
        Task Create(Product product, Guid userId);

        Task Update(Product product);

        Task<Product> Get(Guid id);

        Task Delete(Guid id);

        Task ChangeState(Guid id, ProductState state);

        Task<IEnumerable<Product>> All();

        Task<IEnumerable<Product>> All(Expression<Func<ProductDto, bool>> match);

        Task<IEnumerable<Product>> UserProducts(Guid userId);
    }
}
