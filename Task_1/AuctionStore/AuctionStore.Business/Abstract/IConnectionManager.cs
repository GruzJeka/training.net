﻿namespace AuctionStore.Business.Abstract
{
    public interface IConnectionManager
    {
        void Add(string location);

        void Remove(string location);
    }
}
