﻿using System.Collections.Generic;
using System.Text;

namespace AuctionStore.Business.Helpers
{
    public static class StringConcat
    {
        public static string ConcatAll(this IEnumerable<string> str)
        {
            StringBuilder builder = new StringBuilder();
            foreach (var item in str)
            {
                builder.AppendLine(item);
            }

            return builder.ToString();
        }
    }
}
