﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuctionStore.Business.Abstract;
using AuctionStore.Business.Entities;
using AuctionStore.Business.Infrastructure;
using AuctionStore.DAL.Abstract;
using AuctionStore.DAL.DtoEntities;
using AuctionStore.DAL.EnumsDto;
using AutoMapper;
using NLog;

namespace AuctionStore.Business.Services
{
    public class BidService : IBidService
    {
        private readonly Lazy<IRepository<ProductDto>> _products;

        private readonly Lazy<IRepository<BidDto>> _bids;

        private readonly Lazy<IBidPriceCalculator> _priceCalculator;

        private readonly Lazy<ILogger> _logger;

        public BidService(Lazy<IRepository<ProductDto>> products, Lazy<IRepository<BidDto>> bids, Lazy<IBidPriceCalculator> priceCalculator, Lazy<ILogger> logger)
        {
            _products = products;
            _bids = bids;
            _priceCalculator = priceCalculator;
            _logger = logger;
        }

        public async Task Make(Guid userId, Guid productId)
        {
                ProductDto product = await _products.Value.GetAsync(productId);
            if (IsValid(userId, Mapper.Map<ProductDto, Product>(product)))
            {
                product.BidsQuantity = _bids.Value.Select().Count(b => b.ProductId == productId);

                decimal price = BidPrice(product);

                await _bids.Value.Insert(new BidDto()
                {
                    DateTime = DateTime.Now,
                    Price = price,
                    ProductId = productId,
                    UserId = userId,
                    State = BidState.Leader
                });

                await RefreshBidsSate(product.Id);
            }
        }

        public async Task<Bid> Get(Guid id)
        {
            BidDto bid = await _bids.Value.GetAsync(id);
            if (bid == null)
            {
                throw new ValidationException("Not found bid");
            }

            if (bid.Product == null)
            {
                bid.Product = await _products.Value.GetAsync(bid.ProductId);
            }

            return Mapper.Map<BidDto, Bid>(bid);
        }

        public async Task<IEnumerable<Bid>> UserBids(Guid userId)
        {
            List<BidDto> bids = await _bids.Value.FindAllAsync(p => p.UserId == userId);

            if (bids.Any(b => b.Product == null))
            {
                foreach (var bid in bids)
                {
                    bid.Product = await _products.Value.GetAsync(bid.ProductId);
                }
            }

            return Mapper.Map<ICollection<BidDto>, IEnumerable<Bid>>(bids);
        }

        private async Task RefreshBidsSate(Guid productId)
        {
            IEnumerable<BidDto> bids = await _bids.Value.FindAllAsync(p => p.ProductId == productId);
            if (bids.Count() != 0)
            {
                decimal maxPrice = bids.Max(p => p.Price);
                foreach (var bid in bids)
                {
                    if (bid.Price < maxPrice)
                    {
                        bid.State = BidState.Interrupted;
                    }

                    await _bids.Value.Update(bid);
                }
            }
        }

        private bool IsValid(Guid userId, Product product)
        {
            if (product == null)
            {
                throw new ValidationException("the product does not exist");
            }

            if (!product.IsActual())
            {
                throw new ValidationException("You can not bet on this product");
            }

            if (product.UserId == userId)
            {
                throw new ValidationException("You can not place a lot on the goods");
            }

            return true;
        }

        private decimal BidPrice(ProductDto product)
        {
            decimal price;
            if (product.BidsQuantity == 0)
            {
                price = product.StartPrice;
            }
            else
            {
                price = _priceCalculator.Value.Calculate(product.StartPrice, product.BidsQuantity);
            }

            return price;
        }
    }
}
