﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AuctionStore.Business.Abstract;
using AuctionStore.Business.Entities;
using AuctionStore.Business.Infrastructure;
using AuctionStore.DAL.Abstract;
using AuctionStore.DAL.DtoEntities;
using AuctionStore.DAL.EnumsDto;
using AutoMapper;

namespace AuctionStore.Business.Services
{
    public class ProductService : IProductService
    {
        private readonly Lazy<IRepository<CategoryDto>> _categories;

        private readonly Lazy<IRepository<ProductDto>> _products;

        private readonly Lazy<IRepository<BidDto>> _bids;

        private readonly Lazy<IBidPriceCalculator> _priceCalculator;

        public ProductService(Lazy<IRepository<CategoryDto>> categories, 
            Lazy<IRepository<ProductDto>> products, 
            Lazy<IRepository<BidDto>> bids, 
            Lazy<IBidPriceCalculator> priceCalculator)
        {
            _categories = categories;
            _products = products;
            _bids = bids;
            _priceCalculator = priceCalculator;
        }

        public async Task<Product> Get(Guid id)
        {
            ProductDto product = await _products.Value.GetAsync(id);

            if (product == null)
            {
                throw new ValidationException("Product not found");
            }

            if (product.Category == null)
            {
                product.Category = await _categories.Value.GetAsync(product.CategoryId);
            }

            product.BidsQuantity = _bids.Value.Select().Count(b => b.ProductId == id);

            if (product.BidsQuantity == 0)
            {
                product.BidPrice = product.StartPrice;
            }
            else
            {
                product.BidPrice = _priceCalculator.Value.Calculate(product.StartPrice, product.BidsQuantity);
            }

            return Mapper.Map<ProductDto, Product>(product);
        }

        public async Task<IEnumerable<Product>> All()
        {
            List<ProductDto> products = _products.Value.Select().ToList();

            await SetCategoriesIfNotExists(products);
            SetCalculatedFields(products);

            return Mapper.Map<IEnumerable<ProductDto>, IEnumerable<Product>>(products);
        }

        public async Task<IEnumerable<Product>> All(Expression<Func<ProductDto, bool>> match)
        {
            List<ProductDto> products = await _products.Value.FindAllAsync(match);

            await SetCategoriesIfNotExists(products);
            SetCalculatedFields(products);

            return Mapper.Map<IEnumerable<ProductDto>, IEnumerable<Product>>(products);
        }

        public async Task<IEnumerable<Product>> UserProducts(Guid userId)
        {
            List<ProductDto> products = await _products.Value.FindAllAsync(p => p.UserId == userId);

            await SetCategoriesIfNotExists(products);
            SetCalculatedFields(products);

            return Mapper.Map<ICollection<ProductDto>, IEnumerable<Product>>(products);
        }

        public async Task Create(Product product, Guid userId)
        {
            product.UserId = userId;

            if (await IsValid(product))
            {
                ProductDto entry = Mapper.Map<Product, ProductDto>(product);

                entry.StartDate = DateTime.Now;
                entry.State = ProductState.Draft;

                await _products.Value.Insert(entry);
            }
        }

        public async Task Update(Product product)
        {
            if (product.State != ProductState.Draft)
            {
                throw new ValidationException("can not be updated");
            }

            if (await IsValid(product))
            {
                await _products.Value.Update(Mapper.Map<Product, ProductDto>(product));
            }
        }

        public async Task Delete(Guid id)
        {
            if (_bids.Value.Select().Any(b => b.ProductId == id))
            {
                throw new ValidationException("Unable to remove the product because it has lots");
            }

            ProductDto product = await _products.Value.GetAsync(id);
            await _products.Value.Delete(product);
        }

        public async Task ChangeState(Guid id, ProductState state)
        {
            ProductDto product = await _products.Value.GetAsync(id);

            if (state == ProductState.Sold)
            {
                throw new ValidationException("can not change state because product already sold");
            }

            product.StartDate = DateTime.Now;
            product.State = state;

            await _products.Value.Update(product);
        }

        public void Dispose()
        {
            _products.Value.Dispose();
        }

        private async Task SetCategoriesIfNotExists(List<ProductDto> products)
        {
            if (products.Any(c => c.Category == null))
            {
                foreach (var product in products)
                {
                    product.Category = await _categories.Value.GetAsync(product.CategoryId);
                }
            }
        }

        private void SetCalculatedFields(List<ProductDto> products)
        {
            foreach (var product in products)
            {
                product.BidsQuantity = _bids.Value.Select().Count(b => b.ProductId == product.Id);

                if (product.BidsQuantity == 0)
                {
                    product.BidPrice = product.StartPrice;
                }
                else
                {
                    product.BidPrice = _priceCalculator.Value.Calculate(product.StartPrice, product.BidsQuantity);
                }
            }
        }

        private async Task<bool> IsValid(Product product)
        {
            if (await _categories.Value.GetAsync(product.CategoryId) == null)
            {
                throw new ValidationException("the category does not exist");
            }

            return true;
        }
    }
}
