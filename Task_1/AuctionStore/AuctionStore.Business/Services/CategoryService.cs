﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuctionStore.Business.Abstract;
using AuctionStore.Business.Entities;
using AuctionStore.Business.Infrastructure;
using AuctionStore.DAL.Abstract;
using AuctionStore.DAL.DtoEntities;
using AutoMapper;

namespace AuctionStore.Business.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly Lazy<IRepository<CategoryDto>> _categories;

        private readonly Lazy<IRepository<ProductDto>> _products;

        public CategoryService(Lazy<IRepository<CategoryDto>> categories, Lazy<IRepository<ProductDto>> products)
        {
            _categories = categories;
            _products = products;
        }

        public async Task<Category> Get(Guid id)
        {
            CategoryDto category = await _categories.Value.GetAsync(id);
            if (category == null)
            {
                throw new ValidationException("Not found category");
            }

            return Mapper.Map<CategoryDto, Category>(category);
        }

        public async Task<Category> Get(string name)
        {
            CategoryDto category = await _categories.Value.FindAsync(p => p.Name == name);
            if (category == null)
            {
                throw new ValidationException("Not found category");
            }

            return Mapper.Map<CategoryDto, Category>(category);
        }

        public IEnumerable<Category> All()
        {
            return Mapper.Map<IEnumerable<CategoryDto>, IEnumerable<Category>>(_categories.Value.Select());
        }

        public async Task Make(Category category)
        {
            await _categories.Value.Insert(Mapper.Map<Category, CategoryDto>(category));
        }

        public async Task Update(Category category)
        {
           await _categories.Value.Update(Mapper.Map<Category, CategoryDto>(category));
        }

        public async Task Delete(Guid id)
        {
            if (_products.Value.Select().Any(p => p.CategoryId == id))
            {
                throw new ValidationException("can not be removed because there are products in this category");
            }

            CategoryDto category = await _categories.Value.GetAsync(id);
            await _categories.Value.Delete(category);
        }
    }
}
