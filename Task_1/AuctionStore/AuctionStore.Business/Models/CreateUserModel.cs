﻿using System.Runtime.Serialization;

namespace AuctionStore.Business.Models
{
    [DataContract]
    public class CreateUserModel
    {
        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public string ConfirmPassword { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string TimeZoneId { get; set; }
    }
}
