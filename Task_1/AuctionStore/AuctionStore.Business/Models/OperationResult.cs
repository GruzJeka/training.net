﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace AuctionStore.Business.Models
{
    [DataContract]
    public class OperationResult
    {
        [DataMember]
        public IEnumerable<string> Errors { get; set; }

        [DataMember]
        public bool Success { get; set; }
    }
}
