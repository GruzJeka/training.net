﻿using System.Runtime.Serialization;
using AuctionStore.DAL.EnumsDto;

namespace AuctionStore.Business.Models
{
    [DataContract]
    public class UserProfileModel
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public string TimeZoneId { get; set; }

        [DataMember]
        public Locale Locale { get; set; }
    }
}