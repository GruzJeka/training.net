﻿using System.Runtime.Serialization;

namespace AuctionStore.Business.Models
{
    [DataContract]
    public class LoginModel
    {
        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public string Password { get; set; } 
    }
}