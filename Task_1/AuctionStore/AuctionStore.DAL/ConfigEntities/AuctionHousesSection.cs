﻿using System.Configuration;

namespace AuctionStore.DAL.ConfigEntities
{
    public class AuctionHousesSection : ConfigurationSection
    {
        [ConfigurationProperty("Houses", IsDefaultCollection = false)]
        [ConfigurationCollection(typeof(AuctionHousesCollection),
            AddItemName = "add",
            ClearItemsName = "clear",
            RemoveItemName = "remove")]
        public AuctionHousesCollection AuctionHouses
        {
            get
            {
                AuctionHousesCollection housesCollection =
                    (AuctionHousesCollection)base["Houses"];
                return housesCollection;
            }

            set
            {
                AuctionHousesCollection housesCollection = value;
            }
        }
    }
}
