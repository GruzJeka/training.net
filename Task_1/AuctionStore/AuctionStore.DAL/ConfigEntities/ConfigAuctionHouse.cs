﻿using System.Configuration;

namespace AuctionStore.DAL.ConfigEntities
{
    public class ConfigAuctionHouse : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true, IsKey = true)]
        public string Name
        {
            get
            {
                return (string)this["name"];
            }

            set
            {
                this["name"] = value;
            }
        }

        [ConfigurationProperty("location", IsRequired = true)]
        public string Location
        {
            get
            {
                return (string)this["location"];
            }

            set
            {
                this["location"] = value;
            }
        }

        [ConfigurationProperty("type", IsRequired = true)]
        public string Type
        {
            get
            {
                return (string)this["type"];
            }

            set
            {
                this["type"] = value;
            }
        }
    }
}
