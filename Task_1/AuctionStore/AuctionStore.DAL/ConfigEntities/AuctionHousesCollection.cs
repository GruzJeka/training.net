﻿using System;
using System.Configuration;

namespace AuctionStore.DAL.ConfigEntities
{
    public class AuctionHousesCollection : ConfigurationElementCollection
    {
        public override ConfigurationElementCollectionType CollectionType => ConfigurationElementCollectionType.AddRemoveClearMap;

        public new ConfigAuctionHouse this[string name]
        {
            get
            {
                return (ConfigAuctionHouse)BaseGet(name);
            }
        }

        public ConfigAuctionHouse this[int index]
        {
            get
            {
                return (ConfigAuctionHouse)BaseGet(index);
            }
            
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }

                BaseAdd(index, value);
            }
        }

        public int IndexOf(ConfigAuctionHouse house)
        {
            return BaseIndexOf(house);
        }

        public void Add(ConfigAuctionHouse house)
        {
            BaseAdd(house);
        }

        public void Remove(ConfigAuctionHouse house)
        {
            if (BaseIndexOf(house) >= 0)
            {
                BaseRemove(house.Name);
            }
        }

        public void RemoveAt(int index)
        {
            BaseRemoveAt(index);
        }

        public void Remove(string name)
        {
            BaseRemove(name);
        }

        public void Clear()
        {
            BaseClear();
        }

        protected override void BaseAdd(ConfigurationElement element)
        {
            BaseAdd(element, false);
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new ConfigAuctionHouse();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ConfigAuctionHouse)element).Name;
        }
    }
}
