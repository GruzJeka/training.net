﻿using System.Data.Entity;
using AuctionStore.DAL.Context;

namespace AuctionStore.DAL.DataInitializers
{
    public class EfUserDataInitializer : DropCreateDatabaseAlways<EfUserDbContext>
    {
        protected override void Seed(EfUserDbContext db)
        {
        }
    }
}
