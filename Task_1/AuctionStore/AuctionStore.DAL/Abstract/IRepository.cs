﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace AuctionStore.DAL.Abstract
{
    public interface IRepository<T> : IDisposable where T : class
    {
        Task Insert(T item);

        Task Update(T item);

        Task Delete(T item);

        IQueryable<T> Select();

        Task<T> GetAsync(Guid id);

        Task<T> FindAsync(Expression<Func<T, bool>> match);

        Task<List<T>> FindAllAsync(Expression<Func<T, bool>> match);
    }
}
