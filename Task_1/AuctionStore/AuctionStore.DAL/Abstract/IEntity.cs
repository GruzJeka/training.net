﻿using System;

namespace AuctionStore.DAL.Abstract
{
    public interface IEntity
    {
        Guid Id { get; set; }
    }
}
