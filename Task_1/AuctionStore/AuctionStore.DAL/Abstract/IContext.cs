﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace AuctionStore.DAL.Abstract
{
    public interface IContext<T> where T : IEntity
    {
        string Path { get; }

        Task<ICollection<T>> Deserialize();

        Task Serialize(IEnumerable<T> items);
    }
}
