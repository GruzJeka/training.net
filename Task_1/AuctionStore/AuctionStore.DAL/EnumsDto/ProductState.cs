﻿namespace AuctionStore.DAL.EnumsDto
{
    public enum ProductState
    {
        Draft,
        Selling,
        Banned,
        Sold
    }
}
