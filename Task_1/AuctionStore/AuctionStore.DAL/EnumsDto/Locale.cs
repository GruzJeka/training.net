﻿namespace AuctionStore.DAL.EnumsDto
{
    public enum Locale
    {
        Ru,
        En,
        Blr
    }
}
