﻿namespace AuctionStore.DAL.EnumsDto
{
    public enum BidState
    {
        Interrupted,
        Leader
    }
}
