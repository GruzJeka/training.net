﻿namespace AuctionStore.DAL.EnumsDto
{
    public enum Role
    {
        User,
        Moderator,
        Admin
    }
}
