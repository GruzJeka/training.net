﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using AuctionStore.DAL.Abstract;
using AuctionStore.DAL.Infrastructure;
using Newtonsoft.Json;

namespace AuctionStore.DAL.Context
{
    public class JsonContext<T> : IContext<T> where T : IEntity 
    {
        public JsonContext(string path)
        {
            Path = path;
            if (!File.Exists(path))
            {
                FileStream stream = new FileStream(path, FileMode.Create);
                stream.Close();
            }
        }

        public string Path { get; }

        public async Task<ICollection<T>> Deserialize()
        {
            try
            {
                using (StreamReader reader = File.OpenText(Path))
                {
                    string result = reader.ReadToEnd();
                    ICollection<T> items = JsonConvert.DeserializeObject<ICollection<T>>(result);
                    if (items == null)
                    {
                        return new List<T>();
                    }

                    return items;
                }
            }
            catch (SerializationException)
            {
                throw new ContextException("Error reading from file");
            }
            catch (IOException)
            {
                throw new ContextException("Error reading from file");
            }
        }

        public async Task Serialize(IEnumerable<T> items)
        {
            try
            {
                string result = JsonConvert.SerializeObject(items, Formatting.Indented);
                using (FileStream stream = new FileStream(Path, FileMode.Truncate, FileAccess.Write))
                { 
                    using (StreamWriter writer = new StreamWriter(stream))
                    {
                        await writer.WriteLineAsync(result);
                    }
                }
            }
            catch (SerializationException)
            {
                throw new ContextException("file writing error");
            }
            catch (IOException)
            {
                throw new ContextException("file writing error");
            }
        }
    }
}
