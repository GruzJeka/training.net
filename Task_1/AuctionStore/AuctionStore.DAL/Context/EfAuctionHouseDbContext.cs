﻿using System.Data.Entity;
using AuctionStore.DAL.DataInitializers;
using AuctionStore.DAL.DtoEntities;

namespace AuctionStore.DAL.Context
{
    public class EfAuctionHouseDbContext : DbContext
    {
        public EfAuctionHouseDbContext(string connectionString)
            : base(connectionString)
        {
            Database.SetInitializer(new EfAuctionHouseDataInitializer());
        }

        public virtual DbSet<ProductDto> Products { get; set; }

        public virtual DbSet<BidDto> Bids { get; set; }

        public virtual DbSet<CategoryDto> Categories { get; set; }
    }
}
