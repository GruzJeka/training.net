﻿using System.Data.Entity;
using AuctionStore.DAL.DataInitializers;
using AuctionStore.DAL.DtoEntities;

namespace AuctionStore.DAL.Context
{
    public class EfUserDbContext : DbContext
    {
        public EfUserDbContext(string connectionString)
            : base(connectionString)
        {
            ////Database.SetInitializer(new EfUserDataInitializer()); 
        }

        public virtual DbSet<UserDto> Users { get; set; }
        
        public virtual DbSet<UserRoleDto> Roles { get; set; }

        public virtual DbSet<UserClaimDto> UserClaims { get; set; }
    }
}
