﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AuctionStore.DAL.Abstract;

namespace AuctionStore.DAL.DtoEntities
{
    public class CategoryDto : IEntity
    {
        public CategoryDto()
        {
            Products = new HashSet<ProductDto>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public virtual ICollection<ProductDto> Products { get; set; }
    }
}
