﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AuctionStore.DAL.Abstract;
using AuctionStore.DAL.EnumsDto;

namespace AuctionStore.DAL.DtoEntities
{
    public class BidDto : IEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }

        public DateTime DateTime { get; set; }

        public Guid UserId { get; set; }

        public Guid ProductId { get; set; }

        public virtual ProductDto Product { get; set; }

        public decimal Price { get; set; }

        public BidState State { get; set; }
    }
}
