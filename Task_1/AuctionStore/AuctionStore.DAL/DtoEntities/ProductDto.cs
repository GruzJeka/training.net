﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AuctionStore.DAL.Abstract;
using AuctionStore.DAL.EnumsDto;

namespace AuctionStore.DAL.DtoEntities
{
    public class ProductDto : IEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }

        public Guid CategoryId { get; set; }

        public virtual CategoryDto Category { get; set; }

        public Guid UserId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public ProductState State { get; set; }

        public DateTime StartDate { get; set; }

        public decimal StartPrice { get; set; }

        public long Duration { get; set; }

        public int BidsQuantity { get; set; }

        public decimal BidPrice { get; set; }

        public string ImageData { get; set; }

        public string ImageType { get; set; }
    }
}
