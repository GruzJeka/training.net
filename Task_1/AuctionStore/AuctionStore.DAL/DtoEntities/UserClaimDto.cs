﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AuctionStore.DAL.DtoEntities
{
    public class UserClaimDto
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }

        public Guid UserId { get; set; }

        public virtual UserDto User { get; set; }

        public string Type { get; set; }

        public string Value { get; set; }
    }
}
