﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AuctionStore.DAL.EnumsDto;

namespace AuctionStore.DAL.DtoEntities
{
    public class UserRoleDto
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }

        public Guid UserId { get; set; }

        public Role Role { get; set; }
    }
}
