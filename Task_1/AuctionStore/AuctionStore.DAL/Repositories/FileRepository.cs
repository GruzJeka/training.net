﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AuctionStore.DAL.Abstract;
using AuctionStore.DAL.Infrastructure;
using NLog;

namespace AuctionStore.DAL.Repositories
{
    public class FileRepository<T> : IRepository<T> where T : class, IEntity
    {
        private readonly IContext<T> _context;

        private readonly Lazy<ILogger> _logger;

        public FileRepository(IContext<T> context, Lazy<ILogger> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task Insert(T item)
        {
            item.Id = Guid.NewGuid();
            ICollection<T> items = await ItemsAsync();
            items.Add(item);

            await CommitAsync(items);
        }

        public async Task Update(T item)
        {
            ICollection<T> items = await ItemsAsync();

            T oldItem = items.FirstOrDefault(p => p.Id == item.Id);
            if (oldItem != null)
            {
                items.Remove(oldItem);
                items.Add(item);
                await CommitAsync(items);
            }
        }

        public async Task Delete(T item)
        {
            ICollection<T> items = await ItemsAsync();
            items.Remove(item);

            await CommitAsync(items);
        }

        public IQueryable<T> Select()
        {
            Task<ICollection<T>> items = ItemsAsync();
            return items.Result.AsQueryable();
        }

        public async Task<T> GetAsync(Guid id)
        {
            ICollection<T> items = await ItemsAsync();

            return items.FirstOrDefault(p => p.Id == id);
        }

        public async Task<T> FindAsync(Expression<Func<T, bool>> match)
        {
            ICollection<T> items = await ItemsAsync();

            return items.AsQueryable().FirstOrDefault(match);
        }

        public async Task<List<T>> FindAllAsync(Expression<Func<T, bool>> match)
        {
            ICollection<T> items = await ItemsAsync();

            return items.AsQueryable().ToList();
        }

        public void Dispose()
        {
        }

        public async Task<ICollection<T>> ItemsAsync()
        {
            try
            {
                return await _context.Deserialize();
            }
            catch (ContextException exception)
            {
                _logger.Value.Warn(exception, exception.Message);
                throw;
            }
        }

        public async Task CommitAsync(ICollection<T> items)
        {
            try
            {
                await _context.Serialize(items);
            }
            catch (ContextException exception)
            {
                _logger.Value.Warn(exception, exception.Message);
                throw;
            }
        }
    }
}
