﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AuctionStore.DAL.Abstract;
using NLog;

namespace AuctionStore.DAL.Repositories
{
    public class EfRepository<T> : IRepository<T> where T : class
    {
        private readonly DbContext _dbContext;

        private readonly Lazy<ILogger> _logger;

        private readonly DbSet<T> _dbSet;

        public EfRepository(DbContext dbContext, Lazy<ILogger> logger)
        {
            _dbContext = dbContext;
            _dbSet = dbContext.Set<T>();
            _logger = logger;
        }

        public async Task Insert(T item)
        {
            _dbSet.Add(item);
            await CommitAsync();
        }

        public async Task Update(T item)
        {
            if (item != null)
            {
                _dbSet.AddOrUpdate(item);
                await CommitAsync();
            }
        }

        public async Task Delete(T item)
        {
            _dbSet.Remove(item);
            await CommitAsync();
        }

        public IQueryable<T> Select()
        {
            return _dbSet;
        }

        public async Task<T> GetAsync(Guid id)
        {
            return await _dbSet.FindAsync(id);
        }

        public async Task<T> FindAsync(Expression<Func<T, bool>> match)
        {
            return await _dbSet.FirstOrDefaultAsync(match);
        }

        public async Task<List<T>> FindAllAsync(Expression<Func<T, bool>> match)
        {
            return await _dbSet.Where(match).ToListAsync();
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }

        private async Task CommitAsync()
        {
            try
            {
                await _dbContext.SaveChangesAsync();
            }
            catch (DbUpdateException exception)
            {
                _logger.Value.Warn(exception, exception.Message);
                throw;
            }
            catch (DbEntityValidationException exception)
            {
                _logger.Value.Warn(exception, exception.Message);
                throw;
            }
        }
    }
}
