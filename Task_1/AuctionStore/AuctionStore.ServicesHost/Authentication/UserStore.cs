﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AuctionStore.Business.Entities;
using AuctionStore.DAL.EnumsDto;
using AuctionStore.ServicesHost.Abstract;
using Microsoft.AspNet.Identity;

namespace AuctionStore.ServicesHost.Authentication
{
    public class UserStore : IUserPasswordStore<User>, IUserClaimStore<User>, IUserStore<User>, IUserRoleStore<User> 
    {
        private readonly IUserService _userService;
        private readonly IUserRoleService _roleService;
        private readonly IClaimService _claimService;

        public UserStore(IUserService userService, IUserRoleService roleService, IClaimService claimService)
        {
            _userService = userService;
            _roleService = roleService;
            _claimService = claimService;
        }

        public void Dispose()
        {
        }

        public async Task CreateAsync(User user)
        {
            await _userService.Create(user);
            User createdUser = await _userService.Get(user.UserName);
            await _roleService.AddUserToRole(createdUser.Id, Role.User);
        }

        public Task UpdateAsync(User user)
        {
            this._userService.Update(user);
            return Task.FromResult(user);
        }

        public Task DeleteAsync(User user)
        {
            this._userService.Delete(user.Id);
            return Task.FromResult(user);
        }

        public Task<User> FindByIdAsync(string userId)
        {
            User user = this._userService.Get(Guid.Parse(userId)).Result;
            return Task.FromResult(user);
        }

        public Task<User> FindByNameAsync(string userName)
        {
            return Task.FromResult(this._userService.Get(userName)).Result;
        }

        public Task<string> GetPasswordHashAsync(User user)
        {
            User model = this._userService.Get(user.Id).Result;
            return Task.FromResult(model.Password);
        }

        public Task SetPasswordHashAsync(User user, string passwordHash)
        {
            user.Password = passwordHash;
            return Task.FromResult(user.Password);
        }

        public Task<IList<Claim>> GetClaimsAsync(User user)
        {
            List<Claim> claims = new List<Claim>();
            claims.AddRange(_roleService.GetUserRoles(user.Id).Result.Select(r => new Claim(ClaimTypes.Role, r.Role.ToString())));
            claims.AddRange(_claimService.GetUserClaims(user.Id).Result.Select(c => new Claim(c.Type.ToString(), c.Value)));
           
            return Task.FromResult((IList<Claim>)claims);
        }

        public Task AddToRoleAsync(User user, string roleName)
        {
            this._roleService.AddUserToRole(user.Id, (Role)Enum.Parse(typeof(Role), roleName));
            return Task.FromResult(user);
        }

        public Task RemoveFromRoleAsync(User user, string roleName)
        {
            this._roleService.DeleteUserFromRole(user.Id, (Role)Enum.Parse(typeof(Role), roleName));
            return Task.FromResult(user);
        }

        public Task<IList<string>> GetRolesAsync(User user)
        {
            IEnumerable<UserRole> roles = this._roleService.GetUserRoles(user.Id).Result;
            return Task.FromResult<IList<string>>(roles.Select(x => x.Role.ToString()).ToList());
        }

        public Task<bool> IsInRoleAsync(User user, string roleName)
        {
            IEnumerable<UserRole> roles = this._roleService.GetUserRoles(user.Id).Result;
            return Task.FromResult<bool>(roles.Any(x => x.Role.ToString() == roleName));
        }

        public Task AddClaimAsync(User user, Claim claim)
        {
            _claimService.AddClaim(claim.Type, claim.Value, user.Id);
            return Task.FromResult(0);
        }

        public Task RemoveClaimAsync(User user, Claim claim)
        {
            _claimService.RemoveClaim(claim.Type, claim.Value, user.Id);
            return Task.FromResult(0);
        }

        public Task<bool> HasPasswordAsync(User user)
        {
            return Task.FromResult(true);
        }
    }
}