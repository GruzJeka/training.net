﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.ServiceModel;
using System.Threading.Tasks;
using AuctionStore.Business.Entities;
using AuctionStore.Business.Models;
using AuctionStore.DAL.EnumsDto;
using AuctionStore.ServicesHost.Abstract;
using AuctionStore.ServicesHost.Contracts;
using Microsoft.AspNet.Identity;

namespace AuctionStore.ServicesHost
{
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class WcfUserService : IWcfUserService
    {
        private readonly UserManager<User> _userManager;

        private readonly Lazy<IUserService> _userStore;

        public WcfUserService(UserManager<User> userManager, Lazy<IUserService> userStore)
        {
            _userManager = userManager;
            _userStore = userStore;
        }

        public async Task<OperationResult> AddClaimAsync(string userId, Claim claim)
        {
            User user = await _userManager.FindByIdAsync(userId);
            if (user != null)
            {
                IdentityResult result = await _userManager.AddClaimAsync(userId, claim);
                return new OperationResult()
                {
                    Errors = result.Errors,
                    Success = result.Succeeded
                };
            }

            return new OperationResult()
            {
                Errors = new List<string>() { "User not found" },
                Success = false
            };
        }

        public async Task<OperationResult> RemoveClaimAsync(string userId, Claim claim)
        {
            User user = await _userManager.FindByIdAsync(userId);
            if (user != null)
            {
                IdentityResult result = await _userManager.RemoveClaimAsync(userId, claim);
                return new OperationResult()
                {
                    Errors = result.Errors,
                    Success = result.Succeeded
                };
            }

            return new OperationResult()
            {
                Errors = new List<string>() { "User not found" },
                Success = false
            };
        }

        public async Task<OperationResult> ChangePasswordAsync(string userId, string newPassword)
        {
            User user = await _userManager.FindByIdAsync(userId);
            if (user != null)
            {
                string passwordHash = _userManager.PasswordHasher.HashPassword(newPassword);
                user.Password = passwordHash;
                IdentityResult result = await _userManager.UpdateAsync(user);
                return new OperationResult()
                {
                    Errors = result.Errors,
                    Success = result.Succeeded
                };
            }

            return new OperationResult()
            {
                Errors = new List<string> { "User not found" },
                Success = false
            };
        }

        public async Task<IEnumerable<User>> GetUsers()
        {
            return await _userStore.Value.All();
        }

        public async Task<IList<Claim>> GetClaimsAsync(string userId)
        {
            return await _userManager.GetClaimsAsync(userId);
        }

        public async Task<OperationResult> DeleteUserAsync(string userId)
        {
            User user = await _userManager.FindByIdAsync(userId);
            if (user != null)
            {
                IdentityResult identityResult = await _userManager.DeleteAsync(user);

                return new OperationResult()
                {
                    Errors = identityResult.Errors,
                    Success = identityResult.Succeeded
                };
            }

            return new OperationResult()
            {
                Success = false,
                Errors = new List<string>() { "User not found" }
            };
        }

        public async Task<OperationResult> RemoveFromRoleAsync(string userId, Role role)
        {
            IdentityResult idnetity = await _userManager.RemoveFromRoleAsync(userId, role.ToString());

            return new OperationResult()
            {
                Errors = idnetity.Errors,
                Success = idnetity.Succeeded
            };
        }

        public async Task<OperationResult> AddToRoleAsync(string userId, Role role)
        {
            IdentityResult idnetity = await _userManager.AddToRoleAsync(userId, role.ToString());

            return new OperationResult()
            {
                Errors = idnetity.Errors,
                Success = idnetity.Succeeded
            };
        }

        public async Task<User> FindUserByNameAsync(string name)
        {
            return await _userManager.FindByNameAsync(name);
        }

        public async Task<OperationResult> UpdateUserProfileAsync(UserProfileModel model)
        {
            User user = await _userManager.FindByIdAsync(model.Id);
            user.TimeZoneId = model.TimeZoneId;

            IdentityResult result = await _userManager.UpdateAsync(user);

            return new OperationResult()
            {
                Errors = result.Errors,
                Success = result.Succeeded
            };
        }

        public async Task<User> FindUserByIdAsync(string id)
        {
            return await _userManager.FindByIdAsync(id);
        }

        public async Task<User> FindAsync(string login, string password)
        {
            return await _userManager.FindAsync(login, password);
        }

        public async Task<OperationResult> RegisterAsync(User user)
        {
            IdentityResult result = await _userManager.CreateAsync(user, user.Password);

            return new OperationResult()
            {
                Errors = result.Errors,
                Success = result.Succeeded
            };
        }

        public async Task<ClaimsIdentity> GetClaimsIdentityAsync(string userId)
        {
            User user = await _userManager.FindByIdAsync(userId);
            return await _userManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
        }
    }
}
