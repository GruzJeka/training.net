﻿using System.Data.Entity;
using AuctionStore.Business.Entities;
using AuctionStore.DAL.Abstract;
using AuctionStore.DAL.Context;
using AuctionStore.DAL.DtoEntities;
using AuctionStore.DAL.Repositories;
using AuctionStore.ServicesHost.Abstract;
using AuctionStore.ServicesHost.Authentication;
using AuctionStore.ServicesHost.Services;
using Autofac;
using Microsoft.AspNet.Identity;
using NLog;

namespace AuctionStore.ServicesHost.Infrastructure
{
    public class AutofacContainerBuilder
    {
        public static IContainer Build()
        {
            var builder = new ContainerBuilder();

            Logger logger = LogManager.GetCurrentClassLogger();
            builder.RegisterInstance(logger).As<ILogger>();

            ////Contexts
            builder.RegisterType<EfUserDbContext>().As<DbContext>().WithParameter("connectionString", "UsersDataBaseConnection");

            ////Repositories
            builder.RegisterType<EfRepository<UserClaimDto>>().As<IRepository<UserClaimDto>>();
            builder.RegisterType<EfRepository<UserDto>>().As<IRepository<UserDto>>();
            builder.RegisterType<EfRepository<UserRoleDto>>().As<IRepository<UserRoleDto>>();

            ////Services
            builder.RegisterType<UserService>().As<IUserService>();
            builder.RegisterType<UserRoleService>().As<IUserRoleService>();
            builder.RegisterType<ClaimService>().As<IClaimService>();
            builder.RegisterType<UserStore>().As<IUserStore<User>>();
            builder.RegisterType<UserManager<User>>().As<UserManager<User>>();
            builder.RegisterType<WcfUserService>();

            return builder.Build();
        }
    }
}