﻿using System;
using AuctionStore.Business.Entities;
using AuctionStore.DAL.ConfigEntities;
using AuctionStore.DAL.DtoEntities;
using AutoMapper;

namespace AuctionStore.ServicesHost.Infrastructure
{
    public class WcfAutoMap
    {
        public static void RegisterMaps()
        {
            #pragma warning disable 618
            ////Entities
            Mapper.CreateMap<Category, CategoryDto>();
            Mapper.CreateMap<CategoryDto, Category>();

            Mapper.CreateMap<User, UserDto>();
            Mapper.CreateMap<UserDto, User>();

            Mapper.CreateMap<Product, ProductDto>()
                .ForMember("Duration", opt => opt.MapFrom(c => c.Duration.Ticks));
            Mapper.CreateMap<ProductDto, Product>()
                .ForMember("Duration", opt => opt.MapFrom(c => TimeSpan.FromTicks(c.Duration)));

            Mapper.CreateMap<UserRole, UserRoleDto>();
            Mapper.CreateMap<UserRoleDto, UserRole>();

            Mapper.CreateMap<UserClaim, UserClaimDto>();
            Mapper.CreateMap<UserClaimDto, UserClaim>();

            ////ConfigEntities
            Mapper.CreateMap<AuctionHouse, ConfigAuctionHouse>();
            Mapper.CreateMap<ConfigAuctionHouse, AuctionHouse>();

            #pragma warning restore 618
        }
    }
}
