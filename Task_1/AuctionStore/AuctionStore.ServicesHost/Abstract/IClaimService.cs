﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AuctionStore.Business.Entities;

namespace AuctionStore.ServicesHost.Abstract
{
    public interface IClaimService
    {
        Task<IEnumerable<UserClaim>> GetUserClaims(Guid userId);

        Task AddClaim(string type, string value, Guid userId);

        Task RemoveClaim(string type, string value, Guid userId);
    }
}
