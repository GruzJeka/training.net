﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AuctionStore.Business.Entities;

namespace AuctionStore.ServicesHost.Abstract
{
    public interface IUserService
    {
        Task<IEnumerable<User>> All();

        Task Create(User user);

        Task Update(User user);

        Task Delete(Guid id);

        Task<User> Get(Guid id);

        Task<User> Get(string userName);

        bool IsUserExists(string userName);
    }
}
