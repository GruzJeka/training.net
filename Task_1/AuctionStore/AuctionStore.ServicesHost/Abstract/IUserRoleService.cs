﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AuctionStore.Business.Entities;
using AuctionStore.DAL.EnumsDto;

namespace AuctionStore.ServicesHost.Abstract
{
    public interface IUserRoleService
    {
        Task AddUserToRole(Guid userId, Role role);

        Task DeleteUserFromRole(Guid userId, Role role);

        Task<IEnumerable<UserRole>> GetUserRoles(Guid userId);
    }
}
