﻿using System.Collections.Generic;
using System.Security.Claims;
using System.ServiceModel;
using System.Threading.Tasks;
using AuctionStore.Business.Entities;
using AuctionStore.Business.Models;
using AuctionStore.DAL.EnumsDto;

namespace AuctionStore.ServicesHost.Contracts
{
    [ServiceContract]
    public interface IWcfUserService
    {
        [OperationContract]
        Task<IEnumerable<User>> GetUsers();

        [OperationContract]
        Task<IList<Claim>> GetClaimsAsync(string userId);

        [OperationContract]
        Task<OperationResult> RegisterAsync(User user);

        [OperationContract]
        Task<User> FindAsync(string login, string password);

        [OperationContract]
        Task<User> FindUserByIdAsync(string id);

        [OperationContract]
        Task<User> FindUserByNameAsync(string name);

        [OperationContract]
        Task<OperationResult> UpdateUserProfileAsync(UserProfileModel model);

        [OperationContract]
        Task<OperationResult> AddToRoleAsync(string userId, Role role);

        [OperationContract]
        Task<OperationResult> RemoveFromRoleAsync(string userId, Role role);

        [OperationContract]
        Task<OperationResult> DeleteUserAsync(string userId);

        [OperationContract]
        Task<ClaimsIdentity> GetClaimsIdentityAsync(string userId);

        [OperationContract]
        Task<OperationResult> AddClaimAsync(string userId, Claim claim);

        [OperationContract]
        Task<OperationResult> RemoveClaimAsync(string userId, Claim claim);

        [OperationContract]
        Task<OperationResult> ChangePasswordAsync(string userId, string newPassword);
    }
}
