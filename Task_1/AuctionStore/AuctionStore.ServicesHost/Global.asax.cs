﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using AuctionStore.Business.Infrastructure;
using AuctionStore.ServicesHost.Infrastructure;
using Autofac.Integration.Wcf;

namespace AuctionStore.ServicesHost
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            AutofacHostFactory.Container = AutofacContainerBuilder.Build();
            WcfAutoMap.RegisterMaps();
        }
    }
}