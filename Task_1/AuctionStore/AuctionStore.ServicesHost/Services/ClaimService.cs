﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuctionStore.Business.Abstract;
using AuctionStore.Business.Entities;
using AuctionStore.DAL.Abstract;
using AuctionStore.DAL.DtoEntities;
using AuctionStore.ServicesHost.Abstract;
using AutoMapper;

namespace AuctionStore.ServicesHost.Services
{
    public class ClaimService : IClaimService
    {
        private readonly Lazy<IRepository<UserClaimDto>> _claims;

        public ClaimService(Lazy<IRepository<UserClaimDto>> claims)
        {
            _claims = claims;
        }

        public async Task<IEnumerable<UserClaim>> GetUserClaims(Guid userId)
        {
            ICollection<UserClaimDto> claims = await _claims.Value.FindAllAsync(p => p.UserId == userId);
            return Mapper.Map<ICollection<UserClaimDto>, IEnumerable<UserClaim>>(claims);
        }

        public async Task AddClaim(string type, string value, Guid userId)
        {
           await _claims.Value.Insert(new UserClaimDto() { Type = type, UserId = userId, Value = value });
        }

        public async Task RemoveClaim(string type, string value, Guid userId)
        {
            UserClaimDto claim = await _claims.Value.FindAsync(c => c.UserId == userId && c.Type == type && c.Value == value);

            if (claim != null)
            {
               await _claims.Value.Delete(claim);
            }
        }
    }
}
