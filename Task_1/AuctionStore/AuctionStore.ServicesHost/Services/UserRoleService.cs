﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AuctionStore.Business.Entities;
using AuctionStore.Business.Infrastructure;
using AuctionStore.DAL.Abstract;
using AuctionStore.DAL.DtoEntities;
using AuctionStore.DAL.EnumsDto;
using AuctionStore.ServicesHost.Abstract;
using AutoMapper;

namespace AuctionStore.ServicesHost.Services
{
    public class UserRoleService : IUserRoleService
    {
        private readonly IRepository<UserRoleDto> _userRoles;

        private readonly Lazy<IRepository<UserDto>> _users;

        public UserRoleService(IRepository<UserRoleDto> userRoles, Lazy<IRepository<UserDto>> users)
        {
            _userRoles = userRoles;
            _users = users;
        }

        public async Task AddUserToRole(Guid userId, Role role)
        {
             UserDto user = await _users.Value.GetAsync(userId);
            if (user == null)
            {
                throw new ValidationException("Not found user");
            }

            UserRoleDto userRole = new UserRoleDto()
            {
                Role = role,
                UserId = userId
            };

           await _userRoles.Insert(userRole);
        }

        public async Task DeleteUserFromRole(Guid userId, Role role)
        {
            UserDto user = await _users.Value.GetAsync(userId);
            if (user == null)
            {
                throw new ValidationException("Not found user");
            }

            UserRoleDto userRole = await _userRoles.FindAsync(u => u.UserId == userId && u.Role == role);

            if (userRole != null)
            {
               await _userRoles.Delete(userRole);
            }
            else
            {
                throw new ValidationException("role can not be removed");
            }
        }

        public async Task<IEnumerable<UserRole>> GetUserRoles(Guid userId)
        {
            ICollection<UserRoleDto> role = await _userRoles.FindAllAsync(p => p.UserId == userId);
            return Mapper.Map<ICollection<UserRoleDto>, IEnumerable<UserRole>>(role);
        }
    }
}
