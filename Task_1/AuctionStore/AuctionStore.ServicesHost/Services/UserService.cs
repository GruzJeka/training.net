﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuctionStore.Business.Entities;
using AuctionStore.Business.Infrastructure;
using AuctionStore.DAL.Abstract;
using AuctionStore.DAL.DtoEntities;
using AuctionStore.ServicesHost.Abstract;
using AutoMapper;

namespace AuctionStore.ServicesHost.Services
{
    public class UserService : IUserService
    {
        private readonly Lazy<IRepository<UserDto>> _users;

        private readonly Lazy<IRepository<UserRoleDto>> _roles;

        public UserService(Lazy<IRepository<UserDto>> users, Lazy<IRepository<UserRoleDto>> roles)
        {
            _users = users;
            _roles = roles;
        }

        public async Task<User> Get(Guid id)
        {
            UserDto user = await _users.Value.GetAsync(id);

            if (user == null)
            {
                throw new ValidationException("user not found");
            }

            if (user.UserRoles == null)
            {
                user.UserRoles = await _roles.Value.FindAllAsync(p => p.UserId == id);
            }

            return Mapper.Map<UserDto, User>(user);
        }

        public async Task<User> Get(string userName)
        {
            UserDto user = _users.Value.Select().FirstOrDefault(u => u.UserName.Equals(userName));

            if (user == null)
            {
                return null;
            }

            if (user.UserRoles == null)
            {
                user.UserRoles = await _roles.Value.FindAllAsync(u => u.UserId == user.Id);
            }

            return Mapper.Map<UserDto, User>(user);
        }

        public async Task<IEnumerable<User>> All()
        {
            IEnumerable<UserDto> users = _users.Value.Select().ToList();

            if (users.Any(u => u.UserRoles == null))
            {
                foreach (var user in users)
                {
                    user.UserRoles = await _roles.Value.FindAllAsync(u => u.UserId == user.Id);
                }
            }

            return Mapper.Map<IEnumerable<UserDto>, IEnumerable<User>>(users);
        }

        public async Task Create(User user)
        {
            if (IsUserExists(user.UserName))
            {
                throw new ValidationException("user with that username exists");
            }

            if (user.TimeZoneId == null)
            {
                user.TimeZoneId = TimeZoneInfo.Local.Id;
            }

            user.RegistrationDate = DateTime.Now;

            await _users.Value.Insert(Mapper.Map<User, UserDto>(user));
        }

        public async Task Update(User user)
        {
            if (_users.Value.Select().Any(u => u.UserName == user.UserName && u.Id != user.Id))
            {
                throw new ValidationException("user with that username exists");
            }

           await _users.Value.Update(Mapper.Map<User, UserDto>(user));
        }

        public async Task Delete(Guid id)
        {
            UserDto user = await _users.Value.GetAsync(id);
            await _users.Value.Delete(user);
        }

        public bool IsUserExists(string userName)
        {
            return _users.Value.Select().Any(u => u.UserName == userName);
        }
    }
}
